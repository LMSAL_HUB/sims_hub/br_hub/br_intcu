# LMSAL_HUB/sims_hub/br_hub/br_intcu

Is a library for solar physics focused on interfacing with Bifrost numerical codes, 
analysis tools, projects and visualization tools for numerical simulations from 
the Lockheed Martin Solar and Astrophysical Lab  (LMSAL).

The library is a collection of different scripts and classes with varying degrees of 
portability and usefulness.

# Dependencies
Before attempting to install sims_py you need the following:

 * [Python](http://www.python.org) (2.7.x, 3.4.x or later)
 * [Astropy](http://astropy.org)
 * [NumPy](http://numpy.scipy.org/)
 * [SciPy](http://www.scipy.org/)
 * [helita](http://github.com/ITA-Solar/helita.git)
 * [Sunpy](https://sunpy.org)

The following packages are also recommended to take advantage of all the features:

* [Matplotlib](http://matplotlib.sourceforge.net/) (1.1+)
* [netCDF4](https://unidata.github.io/netcdf4-python/)
* [CUDA](https://developer.nvidia.com/cuda-zone)
* [pycuda](https://developer.nvidia.com/pycuda)
* [Chiantipy](http://chiantipy.sourceforge.net)
* [beautifulsoup4](http://www.crummy.com/software/BeautifulSoup/)
* [LMSAL_HUB/sims_hub/bifrost_hub/br_cuda](https://gitlab.com/LMSAL_HUB/sims_hub/br_hub/br_cuda) 

Most of the above Python packages are available through Anaconda, and that is the 
recommended way of setting up your Python distribution.

# Installation
Next, use git to grab the latest version of sims_hub:

      mkdir -p LMSAL_HUB/sims_hub/br_hub
      cd LMSAL_HUB/sims_hub/br_hub
      git clone https://gitlab.com/LMSAL_HUB/sims_hub/br_hub/br_intcu.git
      cd br_intcu
      python setup.py install

### Non-root install

If you do not have write permission to your Python packages directory, use the following 
option with setup.py:

      python setup.py install --user

This will install br_py under your home directory (typically ~/.local).

## Developer install

If you want to install br_py but also actively change the code or contribute 
to its development, it is recommended that you do a developer install instead:

      python setup.py develop

This will set up the package such as the source files used are from the git 
repository that you cloned (only a link to it is placed on the Python packages 
directory). Can also be combined with the --user flag for local installs.

## Install CUDA: 

Follow download and install steps in http://developer.nvidia.com/cuda-downloads

add in the .cshrc: 

       setenv LMSAL_HUB path
       source $LMSAL_HUB/sims_hub/br_hub/br_cuda/cuda.csh

