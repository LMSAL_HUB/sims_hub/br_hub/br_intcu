from __future__ import print_function

import numpy as np

#from PyQt4 import Qt
#from PyQt4 import QtCore
#from PyQt4 import QtGui

from PyQt5 import QtGui
from PyQt5 import Qt
from PyQt5 import QtCore
from PyQt5 import QtWidgets
from PyQt5.QtCore import pyqtSignal

#from PyQt4 import *
#from PyQt4 import QtGui

import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

from matplotlib import cm, colors
from matplotlib.image import NonUniformImage
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar

import os.path
from string import Template
from br_ioni import spectanalys, CC, noisify_spectra

#from Tkinter import Tk

#import tkFileDialog
import sys

#Tk().withdraw()

class Mode(object):
    intensity = 'Intensity'
    doppler_shift = 'Doppler Shift'
    width = 'FWHM'
    asym = 'Asymmetries'


class MyPopup(Qt.QWidget):
    def __init__(self):
        Qt.QWidget.__init__(self)

    def paintEvent(self, e):
        dc = Qt.QPainter(self)
        dc.drawLine(0, 0, 100, 100)
        dc.drawLine(100, 0, 0, 100)

class RenderGUI(QtWidgets.QDialog):
#class RenderGUI():
    rend = None
    initialized = False
    def __init__(self, rend, parent=None):

        super(RenderGUI, self).__init__(parent)

        self.rend = rend
        self.setdefaults()

        self.distance_per_pixel = self.rend.distance_per_pixel
        self.stepsize = self.rend.stepsize

        self.x_pixel_offset = rend.x_pixel_offset
        self.y_pixel_offset = rend.y_pixel_offset
        self.snap = self.rend.snap

        self.channellist = [os.path.basename(os.path.splitext(a)[0]) for a in self.rend.channellist()]

        self.figure = plt.figure()
        self.canvas = FigureCanvas(self.figure)
        self.toolbar = NavigationToolbar(self.canvas, self)


        self.timeslider = QtGui.QScrollBar(Qt.Qt.Horizontal)
        self.timeslider.setFocusPolicy(QtCore.Qt.NoFocus)
        self.timerange()
        self.connect(self.timeslider, QtCore.SIGNAL("valueChanged(int)"), self.update)
        timew= QtGui.QVBoxLayout()
        self.timelabel = QtGui.QLabel("Snapshot #:"+str(self.timeslider.value()))
        self.connect(self.timeslider, QtCore.SIGNAL("valueChanged(int)"), self.changelable)
        timew.addWidget(self.timelabel)
        timew.addWidget(self.timeslider)

        altlabel = QtGui.QLabel("Altitude")
        self.altslider = QtGui.QScrollBar(Qt.Qt.Vertical)
        self.altslider.setFocusPolicy(QtCore.Qt.NoFocus)
        self.altituderange()
        self.connect(self.altslider, QtCore.SIGNAL("valueChanged(int)"), self.update)
        altw= QtGui.QVBoxLayout()
        altw.addWidget(altlabel)
        altw.addWidget(self.altslider)

        azlabel = QtGui.QLabel("Azimuth")
        self.azslider = QtGui.QScrollBar(Qt.Qt.Horizontal)
        self.azslider.setFocusPolicy(QtCore.Qt.NoFocus)
        self.azimuthrange()
        self.connect(self.azslider, QtCore.SIGNAL("valueChanged(int)"), self.update)
        azw= QtGui.QVBoxLayout()
        azw.addWidget(azlabel)
        azw.addWidget(self.azslider)

        self.menubox = QtGui.QHBoxLayout()
        self.RenderSettings = QtGui.QPushButton('Render Set.',self)
        self.RenderSettings.setGeometry(Qt.QRect(0,0,100,30))
        self.connect(self.RenderSettings, Qt.SIGNAL("clicked()"), self.dorenderw)

        self.DisplaySetting = QtGui.QPushButton('Display Set.',self)
        self.DisplaySetting.setGeometry(Qt.QRect(0,0,100,30))
        self.connect(self.DisplaySetting, Qt.SIGNAL("clicked()"), self.dosettingw)

        self.AsymSetting = QtGui.QPushButton('Asymm. Set.',self)
        self.AsymSetting.setGeometry(Qt.QRect(0,0,100,30))
        self.connect(self.AsymSetting, Qt.SIGNAL("clicked()"), self.doasymw)

        self.MultiSetting = QtGui.QPushButton('Multi Set.',self)
        self.MultiSetting.setGeometry(Qt.QRect(0,0,100,30))
        self.connect(self.MultiSetting, Qt.SIGNAL("clicked()"), self.domultiw)

        self.SaveSpectra = QtGui.QPushButton('Save Spectra',self)
        self.SaveSpectra.setGeometry(Qt.QRect(0,0,100,30))
        self.connect(self.SaveSpectra, Qt.SIGNAL("clicked()"), self.dossavew)

        self.SaveRange = QtGui.QPushButton('Save Range',self)
        self.SaveRange.setGeometry(Qt.QRect(0,0,100,30))
        self.connect(self.SaveRange, Qt.SIGNAL("clicked()"), self.dorsavew)

        self.Help = QtGui.QPushButton('Help',self)
        self.Help.setGeometry(Qt.QRect(0,0,100,30))
        self.connect(self.Help, Qt.SIGNAL("clicked()"), self.dohelpw)

        self.menubox.addWidget(self.RenderSettings)
        self.menubox.addWidget(self.DisplaySetting)
        self.menubox.addWidget(self.AsymSetting)
        self.menubox.addWidget(self.MultiSetting)
        self.menubox.addWidget(self.SaveSpectra)
        self.menubox.addWidget(self.SaveRange)
        self.menubox.addWidget(self.Help)

        self.hbox = QtGui.QHBoxLayout()

        self.check_abs = QtGui.QCheckBox('Absolute')
        self.check_abs.stateChanged.connect(self.update_display)

        self.check_bw = QtGui.QCheckBox('B&W')
        self.check_bw.stateChanged.connect(self.update_display)

        self.check_log = QtGui.QCheckBox('Log')
        self.check_log.stateChanged.connect(self.update_display)

        self.check_defminmax = QtGui.QCheckBox('Adjust min/max')
        self.check_defminmax.setChecked(True)
        self.check_defminmax.stateChanged.connect(self.update_display)

        self.check_opa = QtGui.QCheckBox('Opacity')
        self.check_opa.stateChanged.connect(self.select_opacity)

        self.check_rev = QtGui.QCheckBox('Reverse')
        self.check_rev.stateChanged.connect(self.select_reverse)

        axislabel = QtGui.QLabel("LOS")
        self.check_axis =  QtGui.QComboBox(self)
        self.check_axis.addItem('x')
        self.check_axis.addItem('y')
        self.check_axis.addItem('z')
        self.check_axis.activated[str].connect(self.select_axis)
        axisw= QtGui.QHBoxLayout()
        axisw.addWidget(axislabel)
        axisw.addWidget(self.check_axis)

        self.hbox.addWidget(self.check_abs)
        self.hbox.addWidget(self.check_log)
        self.hbox.addWidget(self.check_bw)
        self.hbox.addWidget(self.check_defminmax)
        self.hbox.addWidget(self.check_opa)
        self.hbox.addWidget(self.check_rev)
        self.hbox.addLayout(axisw)

        self.imgbox = QtGui.QHBoxLayout()
        self.imgbox.addWidget(self.canvas)
        self.imgbox.addLayout(altw)

        layout = QtGui.QVBoxLayout()
        layout.addLayout(self.menubox)
        layout.addWidget(self.toolbar)
        layout.addLayout(self.hbox)
        layout.addLayout(timew)
        layout.addLayout(self.imgbox)
        layout.addLayout(azw)
        self.setLayout(layout)
        self.wr = None
        self.wd = None
        self.wm = None
        self.wss = None
        self.wsr = None

        #initial update
        #self._saverangedialog = SaveRangeDialog(self, size_hint=(.8, .8), title="Save Range")
        self.initialized = True
        self.update(updatedisplay=True)
        #self.show()

    def setdefaults(self):
        self.azimuth = 20.0
        self.altitude = 20.0
        self.axis = 0
        self.rend_reverse = False
        self.stepsize = 0.0
        self.x_pixel_offset = 0
        self.y_pixel_offset = 0
        self.rend_opacity = False
        self.channel = 0
        self.step = 1
        self.sdomain = self.rend.snap_range[0]
        self.edomain = self.rend.snap_range[1]
        self.rend_mult_il_render = False
        self.rend_curvature = False
        self.rend_along_x = False
        self.log_offset = 1.0
        self.rendermode = Mode.intensity
        self.spect_analyzer = spectanalys.Analyzer()
        self.nlamb = 41
        self.gridsplit = 20
        self.cbsize = (30, 3000)
        self.asym_sep = 10.0
        self.asym_width = 10.0
        self.noise_snr = -1.0
        self.snap_bound_init = self.rend.snap_range[0]
        self.snap_bound_end = self.rend.snap_range[1]
        self.snap_skip = 1
        self.minv = 0.0
        self.maxv = 999999.0
        self.distance_per_pixel=0.01
        '''
        if self.axis == 0:
            self.distance_per_pixel=np.float32(np.max(self.rend.yaxis))/np.float32(len(self.raw_data))
        elif self.axis == 1:
            self.distance_per_pixel=np.float32(np.max(self.rend.xaxis))/np.float32(len(self.raw_data))
        else:
            self.distance_per_pixel=np.float32(np.max(self.rend.xaxis))/np.float32(len(self.raw_data))
        '''
        self.cmaps = [('Seq', ['Blues', 'BuGn', 'BuPu',
                             'GnBu', 'Greens', 'Greys', 'Oranges', 'OrRd',
                             'PuBu', 'PuBuGn', 'PuRd', 'Purples', 'RdPu',
                             'Reds', 'YlGn', 'YlGnBu', 'YlOrBr', 'YlOrRd']),
         ('Seq(2)', ['afmhot', 'autumn', 'bone', 'cool', 'copper',
                             'gist_heat', 'gray', 'hot', 'pink',
                             'spring', 'summer', 'winter']),
         ('Div',      ['BrBG', 'bwr', 'coolwarm', 'PiYG', 'PRGn', 'PuOr',
                             'RdBu', 'RdGy', 'RdYlBu', 'RdYlGn', 'Spectral',
                             'seismic']),
         ('Qual',    ['Accent', 'Dark2', 'Paired', 'Pastel1',
                             'Pastel2', 'Set1', 'Set2', 'Set3']),
         ('Misc',  ['gist_earth', 'terrain', 'ocean', 'gist_stern',
                             'brg', 'CMRmap', 'cubehelix',
                             'gnuplot', 'gnuplot2', 'gist_ncar',
                             'nipy_spectral', 'jet', 'rainbow',
                             'gist_rainbow', 'hsv', 'flag', 'prism'])]
        self.color='jet'
        #self.update(updatedisplay=False)

    def dorenderw(self):

        self.wr = QtGui.QWidget()

        self.wr.setWindowTitle('Render Setup')
        self.wr.setGeometry(Qt.QRect(100, 100, 400, 200))
        self.wr.vbox = QtGui.QVBoxLayout()
        self.wr.v2box = QtGui.QVBoxLayout()

        modellabel = QtGui.QLabel("Mode")
        self.wr.mode = QtGui.QComboBox(self.wr)
        self.wr.mode.addItem('Intensity')
        self.wr.mode.addItem('Doppler Shift')
        self.wr.mode.addItem('FWHM')
        self.wr.mode.addItem('Asymmetries')
        self.wr.mode.activated[str].connect(self.rend_mode)
        modew= QtGui.QHBoxLayout()
        modew.addWidget(modellabel)
        modew.addWidget(self.wr.mode)

        channellabel = QtGui.QLabel("Spec. line")
        self.wr.channel = QtGui.QComboBox(self.wr)
        for a in self.rend.channellist():
            self.wr.channel.addItem(a)
        self.wr.channel.activated[str].connect(self.select_channel)
        channelw= QtGui.QHBoxLayout()
        channelw.addWidget(channellabel)
        channelw.addWidget(self.wr.channel)

        snaplabel = QtGui.QLabel("Snapsht #")
        self.wr.snap = QtGui.QSpinBox()
        self.wr.snap.setMinimum(0)
        self.wr.snap.setMaximum(9999)
        self.wr.snap.setSingleStep(1)
        self.wr.snap.setValue(self.snap)
        snapw= QtGui.QHBoxLayout()
        snapw.addWidget(snaplabel)
        snapw.addWidget(self.wr.snap)

        nlamblabel = QtGui.QLabel("Wavelngth Pix.")
        self.wr.lamb = QtGui.QSpinBox()
        self.wr.lamb.setMinimum(5)
        self.wr.lamb.setMaximum(10001)
        self.wr.lamb.setSingleStep(5)
        self.wr.lamb.setValue(self.nlamb)
        nlambw= QtGui.QHBoxLayout()
        nlambw.addWidget(nlamblabel)
        nlambw.addWidget(self.wr.lamb)

        gridsplitlabel = QtGui.QLabel("Mesh split in #pix LOS")
        self.wr.gridsplit = QtGui.QSpinBox()
        self.wr.gridsplit.setMinimum(5)
        self.wr.gridsplit.setMaximum(10001)
        self.wr.gridsplit.setSingleStep(5)
        self.wr.gridsplit.setValue(self.gridsplit)
        gridsplitw= QtGui.QHBoxLayout()
        gridsplitw.addWidget(gridsplitlabel)
        gridsplitw.addWidget(self.wr.gridsplit)

        altlabel = QtGui.QLabel("Alt")
        self.wr.altitude = QtGui.QDoubleSpinBox()
        self.wr.altitude.setMinimum(-90.0)
        self.wr.altitude.setMaximum(90.0)
        self.wr.altitude.setSingleStep(5.0)
        self.wr.altitude.setValue(self.altitude)
        altw= QtGui.QHBoxLayout()
        altw.addWidget(altlabel)
        altw.addWidget(self.wr.altitude)

        azthlabel = QtGui.QLabel("Azth")
        self.wr.azimuth = QtGui.QDoubleSpinBox()
        self.wr.azimuth.setMinimum(0.0)
        self.wr.azimuth.setMaximum(360.0)
        self.wr.azimuth.setSingleStep(5.0)
        self.wr.azimuth.setValue(self.azimuth)
        azthw= QtGui.QHBoxLayout()
        azthw.addWidget(azthlabel)
        azthw.addWidget(self.wr.azimuth)

        distperpixlabel= QtGui.QLabel("Dist. per Pix")
        self.wr.distperpix = QtGui.QDoubleSpinBox()
        self.wr.distperpix.setMinimum(0.0)
        self.wr.distperpix.setMaximum(100000.0)
        self.wr.distperpix.setSingleStep(1.0)
        self.wr.distperpix.setValue(self.distance_per_pixel)
        distpw= QtGui.QHBoxLayout()
        distpw.addWidget(distperpixlabel)
        distpw.addWidget(self.wr.distperpix)

        stepsizelabel= QtGui.QLabel("Step size")
        self.wr.stepsize = QtGui.QDoubleSpinBox()
        self.wr.stepsize.setMinimum(0.0)
        self.wr.stepsize.setMaximum(100000.0)
        self.wr.stepsize.setSingleStep(0.05)
        self.wr.stepsize.setValue(self.stepsize)
        stepsw= QtGui.QHBoxLayout()
        stepsw.addWidget(stepsizelabel)
        stepsw.addWidget(self.wr.stepsize)

        snrlabel= QtGui.QLabel("S/N")
        self.wr.snr = QtGui.QDoubleSpinBox()
        self.wr.snr.setMinimum(-1.0)
        self.wr.snr.setMaximum(100000.0)
        self.wr.snr.setSingleStep(0.1)
        self.wr.snr.setValue(self.noise_snr)
        snrw= QtGui.QHBoxLayout()
        snrw.addWidget(snrlabel)
        snrw.addWidget(self.wr.snr)

        self.wr.vbox.addLayout(modew)
        self.wr.vbox.addLayout(channelw)
        self.wr.vbox.addLayout(snapw)
        self.wr.vbox.addLayout(nlambw)
        self.wr.vbox.addLayout(gridsplitw)
        self.wr.v2box.addLayout(snrw)
        self.wr.v2box.addLayout(altw)
        self.wr.v2box.addLayout(azthw)
        self.wr.v2box.addLayout(distpw)
        self.wr.v2box.addLayout(stepsw)

        save = QtGui.QPushButton('Save', self.wr)
        save.setGeometry(10, 40, 80, 20)

        def showText():
            if self.snap != self.wr.snap.value():
                self.snap = self.wr.snap.value()
                self.timeslider.setValue(self.snap)
                print('Snapshot # changed to ', self.snap)
            if self.nlamb != self.wr.lamb.value():
                self.nlamb = self.wr.lamb.value()
                print('nlamb # changed to ', self.nlamb)
            if self.altitude != self.wr.altitude.value():
                self.altitude = self.wr.altitude.value()
                self.altslider.setValue(self.altitude)
                print('altitude changed to ', self.altitude)
            if self.azimuth != self.wr.azimuth.value():
                self.azimuth = self.wr.azimuth.value()
                print('azimuth changed to ', self.azimuth)
                self.azslider.setValue(self.azimuth)
            if self.gridsplit != self.wr.gridsplit.value():
                self.gridsplit = self.wr.gridsplit.value()
                print('Mesh split in number of grids along the LOS', self.gridsplit)
            if self.distance_per_pixel != self.wr.distperpix.value():
                self.distance_per_pixel = self.wr.distperpix.value()
                print('distance per pixel changed to ', self.distance_per_pixel)
            if self.stepsize != self.wr.stepsize.value():
                self.stepsize = self.wr.stepsize.value()
                print('step size changed to ', self.stepsize)
            if self.noise_snr != self.wr.snr.value():
                self.noise_snr = self.wr.snr.value()
                print('Noise S/N changed to ', self.noise_snr)
            self.update(updatedisplay=True)
            self.update(updatedisplay=True)
            self.wr.close()
        self.wr.connect(save, QtCore.SIGNAL('clicked()'), showText)

        layouta = QtGui.QVBoxLayout()
        layoutb = QtGui.QHBoxLayout()
        layoutb.addLayout(self.wr.vbox)
        layoutb.addLayout(self.wr.v2box)
        layouta.addLayout(layoutb)
        layouta.addWidget(save)
        self.wr.setLayout(layouta)

        self.wr.show()

    def doasymw(self):

        self.wa=Qt.QWidget()
        self.wa.setWindowTitle('Asym. Setup')
        self.wa.setGeometry(Qt.QRect(100, 100, 400, 200))
        self.wa.vbox = QtGui.QVBoxLayout()

        asymwlabel= QtGui.QLabel("Asym. Width")
        self.wa.asymmwidth = QtGui.QDoubleSpinBox()
        self.wa.asymmwidth.setMinimum(0.0)
        self.wa.asymmwidth.setMaximum(1000.0)
        self.wa.asymmwidth.setSingleStep(1.0)
        self.wa.asymmwidth.setValue(self.asym_width)
        asymww= QtGui.QHBoxLayout()
        asymww.addWidget(asymwlabel)
        asymww.addWidget(self.wa.asymmwidth)

        asymslabel= QtGui.QLabel("Asym. Separation")
        self.wa.asymmsep = QtGui.QDoubleSpinBox()
        self.wa.asymmsep.setMinimum(0.0)
        self.wa.asymmsep.setMaximum(1000.0)
        self.wa.asymmsep.setSingleStep(1.0)
        self.wa.asymmsep.setValue(self.asym_sep)
        asymsw= QtGui.QHBoxLayout()
        asymsw.addWidget(asymslabel)
        asymsw.addWidget(self.wa.asymmsep)

        self.wa.vbox.addLayout(asymww)
        self.wa.vbox.addLayout(asymsw)

        save = QtGui.QPushButton('Save', self.wa)
        save.setGeometry(10, 40, 80, 20)

        def showText():
            if self.asym_width != self.wa.asymmwidth.value():
                self.asym_width = self.wa.asymmwidth.value()
                print('Asym. width changed to ', self.asym_width)
            if self.asym_sep != self.wa.asymmsep.value():
                self.asym_sep = self.wa.asymmsep.value()
                print('Asym. separation changed to ', self.asym_sep)
            self.update(updatedisplay=True)
            self.wa.close()
        self.wa.connect(save, QtCore.SIGNAL('clicked()'), showText)

        layouta = QtGui.QVBoxLayout()
        layouta.addLayout(self.wa.vbox)
        layouta.addWidget(save)
        self.wa.setLayout(layouta)

        self.wa.show()

    def dosettingw(self):

        self.ws=Qt.QWidget()
        self.ws.setWindowTitle('Display Setup')
        self.ws.setGeometry(Qt.QRect(100, 100, 400, 200))
        self.ws.vbox = QtGui.QVBoxLayout()

        channellabel = QtGui.QLabel("Select Colorbar")
        self.ws.colorbar = QtGui.QComboBox(self.ws)
        #self.ws.colorbar.setValue(self.cmaps)
        for a in range(len(self.cmaps)):
            for b in range(len(self.cmaps[a][1])):
                self.ws.colorbar.addItem(self.cmaps[a][0]+'/'+self.cmaps[a][1][b])
        self.ws.color = self.color
        self.ws.colorbar.activated[str].connect(self.select_colorbar)
        colorbarw= QtGui.QHBoxLayout()
        colorbarw.addWidget(channellabel)
        colorbarw.addWidget(self.ws.colorbar)

        minvlabel= QtGui.QLabel("Min Val")
        self.ws.minv = QtGui.QDoubleSpinBox()
        self.ws.minv.setSingleStep(0.1)
        self.ws.minv.setMinimum(-9.e14)
        self.ws.minv.setMaximum(9.e14)
        self.ws.minv.setValue(self.minv)
        minvw= QtGui.QHBoxLayout()
        minvw.addWidget(minvlabel)
        minvw.addWidget(self.ws.minv)

        maxvlabel= QtGui.QLabel("Max Val")
        self.ws.maxv = QtGui.QDoubleSpinBox()
        self.ws.maxv.setSingleStep(0.1)
        self.ws.maxv.setMinimum(-9.e14)
        self.ws.maxv.setMaximum(9.e14)
        self.ws.maxv.setValue(self.maxv)
        maxvw= QtGui.QHBoxLayout()
        maxvw.addWidget(maxvlabel)
        maxvw.addWidget(self.ws.maxv)

        dynlabel= QtGui.QLabel("Dynamic range")
        self.ws.dynamicrange = QtGui.QDoubleSpinBox()
        self.ws.dynamicrange.setMinimum(0.001)
        self.ws.dynamicrange.setMaximum(100.0)
        self.ws.dynamicrange.setSingleStep(0.1)
        self.ws.dynamicrange.setValue(self.log_offset)
        dynw= QtGui.QHBoxLayout()
        dynw.addWidget(dynlabel)
        dynw.addWidget(self.ws.dynamicrange)

        self.ws.vbox.addLayout(colorbarw)
        self.ws.vbox.addLayout(minvw)
        self.ws.vbox.addLayout(maxvw)
        self.ws.vbox.addLayout(dynw)

        save = QtGui.QPushButton('Save', self.ws)
        save.setGeometry(10, 40, 80, 20)

        def showText():
            if self.ws.color != self.color:
                print('color changed to ', self.ws.color)
                self.color = self.ws.color
                self.check_bwresettofalse()
            if self.ws.maxv.value() != self.maxv:
                self.maxv=self.ws.maxv.value()
                self.check_defminmaxresettofalse()
            if self.ws.minv.value() != self.minv:
                self.minv=self.ws.minv.value()
                self.check_defminmaxresettofalse()
            if self.ws.dynamicrange.value() != self.log_offset:
                self.log_offset=self.ws.dynamicrange.value()
                self.check_logresettofalse()
            self.update_display()
            self.ws.close()
        self.ws.connect(save, QtCore.SIGNAL('clicked()'), showText)

        layouta = QtGui.QVBoxLayout()
        layouta.addLayout(self.ws.vbox)
        layouta.addWidget(save)
        self.ws.setLayout(layouta)

        self.ws.show()

    def domultiw(self):

        self.wm=Qt.QWidget()
        self.wm.setWindowTitle('Multi Setup')
        self.wm.setGeometry(Qt.QRect(100, 100, 400, 200))
        self.wm.vbox = QtGui.QVBoxLayout()

        steplabel= QtGui.QLabel("Time step")
        self.wm.timestep = QtGui.QSpinBox()
        self.wm.timestep.setMinimum(0)
        self.wm.timestep.setMaximum(9999)
        self.wm.timestep.setSingleStep(1)
        self.wm.timestep.setValue(self.step)
        stepw= QtGui.QHBoxLayout()
        stepw.addWidget(steplabel)
        stepw.addWidget(self.wm.timestep)

        sdomlabel= QtGui.QLabel("Start domain")
        self.wm.startdomain = QtGui.QSpinBox()
        self.wm.startdomain.setMinimum(0)
        self.wm.startdomain.setMaximum(9999)
        self.wm.startdomain.setSingleStep(1)
        self.wm.startdomain.setValue(self.sdomain)
        sdomw= QtGui.QHBoxLayout()
        sdomw.addWidget(sdomlabel)
        sdomw.addWidget(self.wm.startdomain)

        edomlabel= QtGui.QLabel("End domain")
        self.wm.enddomain = QtGui.QSpinBox()
        self.wm.enddomain.setMinimum(0)
        self.wm.enddomain.setMaximum(9999)
        self.wm.enddomain.setSingleStep(1)
        self.wm.enddomain.setValue(self.edomain)
        edomw= QtGui.QHBoxLayout()
        edomw.addWidget(edomlabel)
        edomw.addWidget(self.wm.enddomain)

        self.wm.curvature = QtGui.QCheckBox('Curvature')
        self.wm.curvature.stateChanged.connect(self.select_rend_curvature)

        self.wm.intalongx = QtGui.QCheckBox('Int along x-axis')
        self.wm.intalongx.stateChanged.connect(self.select_rend_along_x)
        self.wm.runmultisnap = QtGui.QCheckBox('Run Multi Snapshots')
        self.wm.runmultisnap.stateChanged.connect(self.select_rend_mult_il_render)

        self.wm.vbox.addLayout(stepw)
        self.wm.vbox.addLayout(sdomw)
        self.wm.vbox.addLayout(edomw)
        self.wm.vbox.addWidget(self.wm.curvature)
        self.wm.vbox.addWidget(self.wm.intalongx)
        self.wm.vbox.addWidget(self.wm.runmultisnap)

        save = QtGui.QPushButton('Save', self.wm)
        save.setGeometry(10, 40, 80, 20)

        def showText():
            if self.step != self.wm.timestep.value():
                self.step = self.wm.timestep.value()
                print('Asym. width changed to ', self.step)
            if self.sdomain != self.wm.startdomain.value():
                self.sdomain = self.wm.startdomain.value()
                print('Start snashot number changed to ', self.sdomain)
            if self.edomain != self.wm.enddomain.value():
                self.edomain = self.wm.enddomain.value()
                print('End snashot number changed to ', self.edomain)
            self.wm.close()
        self.wm.connect(save, QtCore.SIGNAL('clicked()'), showText)

        layouta = QtGui.QVBoxLayout()
        layouta.addLayout(self.wm.vbox)
        layouta.addWidget(save)
        self.wm.setLayout(layouta)

        self.wm.show()

    def dossavew(self):

        self.wss=Qt.QWidget()
        self.wss.setWindowTitle('Spectral Save')
        self.wss.setGeometry(Qt.QRect(100, 100, 400, 200))
        self.wss.vbox = QtGui.QVBoxLayout()
        self.wss.savefilename = QtGui.QFileDialog.getSaveFileName()#'Open', self.wsr.pathdiag)
        if not self.wss.savefilename:
            return

        snap_rangelabel= QtGui.QLabel("Snap range (min/max/step)")
        self.wss.snap_bound_init = QtGui.QSpinBox()
        self.wss.snap_bound_init.setMinimum(0)
        self.wss.snap_bound_init.setMaximum(9999)
        self.wss.snap_bound_init.setSingleStep(1)
        self.wss.snap_bound_init.setValue(self.snap_bound_init)
        self.wss.snap_bound_end = QtGui.QSpinBox()
        self.wss.snap_bound_end.setMinimum(0)
        self.wss.snap_bound_end.setMaximum(9999)
        self.wss.snap_bound_end.setSingleStep(1)
        self.wss.snap_bound_end.setValue(self.snap_bound_end)
        self.wss.snap_skip = QtGui.QSpinBox()
        self.wss.snap_skip.setMinimum(0)
        self.wss.snap_skip.setMaximum(9999)
        self.wss.snap_skip.setSingleStep(1)
        self.wss.snap_skip.setValue(self.snap_skip)
        snaprw= QtGui.QHBoxLayout()
        snaprw.addWidget(snap_rangelabel)
        snaprw.addWidget(self.wss.snap_bound_init)
        snaprw.addWidget(self.wss.snap_bound_end)
        snaprw.addWidget(self.wss.snap_skip)

        self.wss.chlabbox = QtGui.QHBoxLayout()
        channellabel = QtGui.QLabel("Spectral line list")
        self.wss.chlisbox = QtGui.QVBoxLayout()
        for a in self.rend.channellist():
            channellist = QtGui.QCheckBox(a)
            self.wss.chlisbox.addWidget(channellist)
        self.wss.chlabbox.addWidget(channellabel)
        self.wss.chlabbox.addLayout(self.wss.chlisbox)


        save = QtGui.QPushButton('Save', self.wm)
        save.setGeometry(10, 40, 80, 20)

        def showText():
            self._renderrangefromdialog(self.wss,'il')
            self.wss.close()
        self.wss.connect(save,Qt.SIGNAL('clicked()'), showText)

        layouta = QtGui.QVBoxLayout()
        layouta.addLayout(self.wss.vbox)
        layouta.addLayout(snaprw)
        layouta.addLayout(self.wss.chlabbox)
        layouta.addWidget(save)
        self.wss.setLayout(layouta)
        self.wss.show()


    def dorsavew(self):

        self.wsr=Qt.QWidget()
        self.wsr.setWindowTitle('Range Save')
        self.wsr.setGeometry(Qt.QRect(100, 100, 400, 200))
        self.wsr.vbox = QtGui.QVBoxLayout()
        self.wsr.savefilename = QtGui.QFileDialog.getSaveFileName()#'Open', self.wsr.pathdiag)
        if not self.wsr.savefilename:
            return

        snap_rangelabel= QtGui.QLabel("Snap range (min/max/step)")
        self.wsr.snap_bound_init = QtGui.QSpinBox()
        self.wsr.snap_bound_init.setMinimum(0)
        self.wsr.snap_bound_init.setMaximum(9999)
        self.wsr.snap_bound_init.setSingleStep(1)
        self.wsr.snap_bound_init.setValue(self.snap_bound_init)
        self.wsr.snap_bound_end = QtGui.QSpinBox()
        self.wsr.snap_bound_end.setMinimum(0)
        self.wsr.snap_bound_end.setMaximum(9999)
        self.wsr.snap_bound_end.setSingleStep(1)
        self.wsr.snap_bound_end.setValue(self.snap_bound_end)
        self.wsr.snap_skip = QtGui.QSpinBox()
        self.wsr.snap_skip.setMinimum(0)
        self.wsr.snap_skip.setMaximum(9999)
        self.wsr.snap_skip.setSingleStep(1)
        self.wsr.snap_skip.setValue(self.snap_skip)
        snaprw= QtGui.QHBoxLayout()
        snaprw.addWidget(snap_rangelabel)
        snaprw.addWidget(self.wsr.snap_bound_init)
        snaprw.addWidget(self.wsr.snap_bound_end)
        snaprw.addWidget(self.wsr.snap_skip)

        self.wsr.chlabbox = QtGui.QHBoxLayout()
        channellabel = QtGui.QLabel("Spectral line list")
        self.wsr.chlisbox = QtGui.QVBoxLayout()
        for a in self.rend.channellist():
            channellist = QtGui.QCheckBox(a)
            self.wsr.chlisbox.addWidget(channellist)
        self.wsr.chlabbox.addWidget(channellabel)
        self.wsr.chlabbox.addLayout(self.wsr.chlisbox)

        save = QtGui.QPushButton('Save', self.wm)
        save.setGeometry(10, 40, 80, 20)

        def showText():
            self._renderrangefromdialog(self.wsr,'i')
            self.wsr.close()
        self.wsr.connect(save,Qt.SIGNAL('clicked()'), showText)

        layouta = QtGui.QVBoxLayout()
        layouta.addLayout(self.wsr.vbox)
        layouta.addLayout(snaprw)
        layouta.addLayout(self.wsr.chlabbox)
        layouta.addWidget(save)
        self.wsr.setLayout(layouta)
        self.wsr.show()

    def pathFileBrowse(self):
        file = str(Qt.QFileDialog.getExistingDirectory(self, "Select Directory"))
        self.pathBox.setText(file)

    def dohelpw(self):
        #print "Opening a new popup window..."
        self.wsr=Qt.QLabel("Help info")
        self.wsr.setGeometry(Qt.QRect(100, 100, 400, 200))
        self.wsr.show()

    def changelable(self):
        self.timelabel.setText("Snapshot #:"+str(self.timeslider.value()))

    def rend_mode(self, text):
        self.rendermode = text
        print('changed to: ', self.rendermode)
        #self.update(updatedisplay=True)

    def select_channel(self, text):
        gen = (i for i,x in enumerate(self.rend.channellist()) if x == text)
        for i in gen: self.channel = i
        print('Chanel changed to ', self.channellist[self.channel])


    def check_defminmaxresettofalse(self):
        self.check_defminmax.setChecked(False)

    def check_logresettofalse(self):
        self.check_log.setChecked(False)

    def check_bwresettofalse(self):
        self.check_bw.setChecked(False)

    def select_colorbar(self,text):
        foundcolor=False
        for a in range(len(self.cmaps)):
            for b in range(len(self.cmaps[a][1])):
                if self.cmaps[a][0]+'/'+self.cmaps[a][1][b] == text:
                    self.ws.color=self.cmaps[a][1][b]
                    foundcolor=True
        if foundcolor == False: self.ws.color=self.color

    def select_axis(self, text):
        self.axis = text
        if self.axis == 'x':
            self.altitude=0.0
            self.azimuth=0.0
            self.axis=0
        elif self.axis == 'y':
            self.altitude=0.0
            self.azimuth=90.0
            self.axis=1
        elif self.axis == 'z':
            self.altitude=90.0
            self.azimuth=0.0
            self.axis=2
        self.altslider.setValue(self.altitude)
        self.azslider.setValue(self.azimuth)

        print('Axis changed to ', self.axis)
        self.update(updatedisplay=True)

    def select_reverse(self, text):
        if text is 2:
            self.rend_reverse = True
            print('Reverse is on')
        else:
            self.rend_reverse = False
            print('Reverse is off')
        self.update(updatedisplay=True)

    def select_opacity(self, text):
        if text is 2:
            self.rend_opacity = True
            print('Opacity is on')
        else:
            self.rend_opacity = False
            print('Opacity is off')
        self.update(updatedisplay=True)

    def select_rend_curvature(self, text):
        if text is 2:
            self.rend_curvature = True
            print('Curvature is on')
        else:
            self.rend_curvature = False
            print('Curvature is off')
        self.update(updatedisplay=True)

    def select_rend_along_x(self, text):
        if text is 2:
            self.rend_along_x = True
            print('Integration is along x axis')
        else:
            self.rend_along_x = False
            print('Integration is along y axis')
        self.update(updatedisplay=True)

    def select_rend_mult_il_render(self, text):
        if text is 2:
            self.rend_mult_il_render = True
            print('Multi snapshots is on')
        else:
            self.rend_mult_il_render = False
            print('Multi snapshots is off')
        self.update(updatedisplay=True)

    def keyPressEvent(self, event):

        key = event.key

        if key == Qt.Qt.Key_Right:
            self.timeslider.setValue(self.timeslider.value() + 1)
        elif key == Qt.Qt.Key_Left:
            self.timeslider.setValue(self.timeslider.value() - 1)

    def goBack(self):
        self.param = get_bifrost_param(self.fpath,-1)
        self.b, self.base_name, self.snap_n = get_bifrost_obj(self.fpath,-1, self.check_aux.isChecked())
        self.get_data()
        self.update_display()
        where_str, base_name, snap_n = process_file_name(self.fpath)
        snap_n = str(int(snap_n) - 1)
        self.fpath = where_str + base_name + '_' + snap_n + '.snap'
        pass

    def goNext(self):
        self.param = get_bifrost_param(self.fpath,1)
        self.b, self.base_name, self.snap_n = get_bifrost_obj(self.fpath,1,self.check_aux.isChecked())
        self.get_data()
        self.update_display()
        where_str, base_name, snap_n = process_file_name(self.fpath)
        snap_n = str(int(snap_n) + 1)
        self.fpath = where_str + base_name + '_' + snap_n + '.snap'
        pass

    def comboActivated(self, text):
        self.tag = text
        self.data = self.get_var(self.tag)
        self.update_display()

    def altituderange(self):
         self.altslider.setMinimum(0)
         self.altslider.setMaximum(360)

    def azimuthrange(self):
         self.azslider.setMinimum(-90)
         self.azslider.setMaximum(90)

    def timerange(self):
          self.timeslider.setMinimum(self.rend.snap_range[0])
          self.timeslider.setMaximum(self.rend.snap_range[1])
          self.timeslider.setSingleStep(1)

    def get_data(self):
        self.data = self.arr
        if self.view == 'X-Y':
            self.slide_dimension = 2
        elif self.view == 'X-Z':
            self.slide_dimension = 1
        elif self.view == 'Y-Z':
            self.slide_dimension = 0

        self.slider.setMinimum(0)
        self.slider.setMaximum(self.data.shape[self.slide_dimension]-1)

        if self.slider.value() >= self.data.shape[self.slide_dimension]:
            self.slider.setValue(self.data.shape[self.slide_dimension]-1)
        elif self.slider.value() < 0:
            self.slider.setValue(0)

    def home(self):
        self.toolbar.home()

    def zoom(self):
        self.toolbar.zoom()

    def pan(self):
        self.toolbar.pan()

    def update_display(self):
        plt.clf()
        ax = self.figure.add_subplot(111)
        slice_str = '[?]'
        if self.rendermode == Mode.intensity:
            label = 'Intensity [erg/s/cm2/sr]'
        elif self.rendermode == Mode.doppler_shift:
            label = 'Doppler shift [km/s]'
        elif self.rendermode == Mode.width:
            label = 'Line width at half max [km/s]'
        elif self.rendermode == Mode.asym:
            label = 'Asym Int. [erg/s/cm2/sr]'

        bounds = (np.nanmin(self.raw_data), np.nanmax(self.raw_data))
        #bounds = (np.min(self.raw_data), np.max(self.raw_data))

        image = self.raw_data

        if self.check_abs.isChecked():
            image = np.absolute(image)
            label = "ABS( %s )" % label

        if self.check_log.isChecked():
            minval=np.min(image)
            if minval == 0:
                minvalarr=np.absolute(np.ones((len(self.raw_data),len(self.raw_data[0])))/1.e20)
            elif minval < 0:
                print('Warning var has negative values:', np.min(image))
                minvalarr=np.absolute(np.ones((len(self.raw_data),len(self.raw_data[0])))*minval*1.01)
                print('added ',np.min(minvalarr),'everywhere')
            else:
                minvalarr=np.zeros((len(image),len(image[0])))
            image = np.log10(image+minvalarr)
            label = "Log10( %s )" % label
            self.log_offset = 1.0
        else:
            #self.log_offset = self.ws.dynamicrange.value()
            image=image** self.log_offset

        if self.check_bw.isChecked():
            self.color = 'Greys_r'
        #else:
            #self.color = self.ws.color
        '''if self.view == 'z':
            ax.set_ylabel('Y [Mm]')
            ax.set_xlabel('X [Mm]')
            im = NonUniformImage(ax, interpolation='bilinear', extent=(self.x.min(),self.x.max(),self.y.min(),self.y.max()), cmap=color)
            im.set_data(self.x, self.y, np.fliplr(zip(*image[::-1])))
            ax.images.append(im)
            ax.set_xlim(self.x.min(),self.x.max())
            ax.set_ylim(self.y.min(),self.y.max())
            ax.xaxis.set_major_locator(ticker.MultipleLocator(int(4)))
            ax.yaxis.set_major_locator(ticker.MultipleLocator(int(4)))
        elif self.view == 'y':
            ax.set_ylabel('Z [Mm]')
            ax.set_xlabel('X [Mm]')
            im = NonUniformImage(ax, interpolation='bilinear', extent=(self.x.min(),self.x.max(),self.z.min(),self.z.max()), cmap=color)
            im.set_data(self.x, self.z[::-1], np.flipud(np.fliplr(zip(*image[::-1]))))
            ax.images.append(im)
            ax.set_xlim(self.x.min(),self.x.max())
            ax.set_ylim(self.z.max(),self.z.min())
            ax.xaxis.set_major_locator(ticker.MultipleLocator(int(4)))
            ax.yaxis.set_major_locator(ticker.MultipleLocator(int(2)))
        elif self.view == 'x':
            ax.set_ylabel('Z [Mm]')
            ax.set_xlabel('Y [Mm]')
            im = NonUniformImage(ax, interpolation='bilinear', extent=(self.y.min(),self.y.max(),self.z.min(),self.z.max()), cmap=color)
            im.set_data(self.y, self.z[::-1], np.flipud(np.fliplr(zip(*image[::-1]))))
            ax.images.append(im)
            ax.set_xlim(self.y.min(),self.y.max())
            ax.set_ylim(self.z.max(),self.z.min())
            ax.xaxis.set_major_locator(ticker.MultipleLocator(int(4)))
            ax.yaxis.set_major_locator(ticker.MultipleLocator(int(2)))
        else:
            self.x_pro =
            self.y_pro = self.x*
            ax.set_ylabel('X [Mm] (projected)')
            ax.set_xlabel('Y [Mm] (projected)')
            im = NonUniformImage(ax, interpolation='bilinear', extent=(self.y.min(),self.y.max(),self.z.min(),self.z.max()), cmap=color)
            im.set_data(self.y, self.z[::-1], np.flipud(np.fliplr(zip(*image[::-1]))))
            ax.images.append(im)
            ax.set_xlim(self.y.min(),self.y.max())
            ax.set_ylim(self.z.max(),self.z.min())
            ax.xaxis.set_major_locator(ticker.MultipleLocator(int(4)))
            ax.yaxis.set_major_locator(ticker.MultipleLocator(int(2)))'''
        # im = ax.imshow(image, interpolation='none', origin='lower', cmap=color, extent=extension)
        #ax.text(0.025, 0.025, (r'$\langle  B_{z}  \rangle = %2.2e$'+'\n'+r'$\langle |B_{z}| \rangle = %2.2e$') % (np.average(image),np.average(np.absolute(image))), ha='left', va='bottom', transform=ax.transAxes)
        if self.check_defminmax.isChecked():
            self.minv=np.min(image)
            self.maxv=np.max(image)

        if self.rend.singlaxis:
            if self.axis == 0:
                self.x= self.rend.yaxis
                self.y= self.rend.zaxis
                ax.set_xlabel('Y [Mm]')
                ax.set_ylabel('Z [Mm]')
            elif self.axis == 1:
                self.x= self.rend.xaxis
                self.y= self.rend.zaxis
                ax.set_xlabel('X [Mm]')
                ax.set_ylabel('Z [Mm]')
            else:
                self.x= self.rend.xaxis
                self.y= self.rend.yaxis
                ax.set_xlabel('X [Mm]')
                ax.set_ylabel('Y [Mm]')
        else:
            self.x= np.linspace(0, len(self.raw_data), len(self.raw_data))*self.distance_per_pixel
            self.y= np.linspace(0, len(self.raw_data[0]), len(self.raw_data[0]))*self.distance_per_pixel
            ax.set_xlabel('X [Mm] (projected)')
            ax.set_ylabel('Y [Mm] (projected)')

        minval=np.ones((len(self.raw_data),len(self.raw_data[0])))*self.minv
        maxval=np.ones((len(self.raw_data),len(self.raw_data[0])))*self.maxv
        image=np.maximum(image,minval)
        image=np.minimum(image,maxval)
        im = NonUniformImage(ax, interpolation='bilinear',extent=(self.x.min(),self.x.max(),self.y.min(),self.y.max()), cmap=cm.get_cmap(self.color))

        if self.rend.singlaxis:
            im.set_data(self.x, self.y, np.fliplr(zip(*image[::-1])))
        else:
            im.set_data(self.x, self.y, image)
        ax.images.append(im)
        divider = make_axes_locatable(ax)
        ax.axis('equal')
        ax.set_ylim([np.min(self.y),np.max(self.y)])
        ax.set_xlim([np.min(self.x),np.max(self.x)])
        cax = divider.append_axes("right", size="5%", pad=0.05)
        plt.colorbar(im, cax=cax,label=label)
        self.canvas.draw()

    def update(self, updatedisplay=True):
        '''
        Rerenders stuff and caches it, then updates display if specified
        '''
        if not self.initialized:
            return
        # limit some values

        self.azimuth = self.azslider.value() % 360
        self.altitude = sorted((-90, self.altslider.value(), 90))[1]
        self.snap = sorted(self.rend.snap_range + (self.timeslider.value(),))[1]
        # set values in renderer, and render
        self.rend.distance_per_pixel = self.distance_per_pixel
        self.rend.stepsize = self.stepsize
        self.rend.y_pixel_offset = self.y_pixel_offset
        self.rend.x_pixel_offset = self.x_pixel_offset
        self.rend.set_snap(self.snap)

        # render appropriate data, cache it
        if self.rend_mult_il_render :
            if self.rendermode == Mode.intensity:
                data, _ = self.get_isun_render()
                self.raw_spectra = None
                self.raw_data = data
            else :
                data, dfreqs, ny0, _ = self.get_ilsun_render()
                self.raw_spectra = (noisify_spectra(data, self.noise_snr), dfreqs, ny0)
                self.raw_data = None
                self.spect_analyzer.set_data(*self.raw_spectra)
        else :
            if self.rendermode == Mode.intensity:
                data, _ = self.get_i_render()
                self.raw_spectra = None
                self.raw_data = data
            else :
                data, dfreqs, ny0, _ = self.get_il_render()
                self.raw_spectra = (noisify_spectra(data, self.noise_snr), dfreqs, ny0)
                self.raw_data = None
                self.spect_analyzer.set_data(*self.raw_spectra)
                if self.rend.singlaxis:
                    data, dfreqs, ny0, _ = self.get_il_render()
                    self.raw_spectra = (noisify_spectra(data, self.noise_snr), dfreqs, ny0)
                    self.raw_data = None
                    self.spect_analyzer.set_data(*self.raw_spectra)

        if self.rendermode == Mode.doppler_shift:
            self.raw_data = self.spect_analyzer.quad_regc()
            self.raw_data *= -CC / 1e3 / self.spect_analyzer.center_freq  # convert to km/s
        elif self.rendermode == Mode.width:
            self.raw_data = self.spect_analyzer.fwhm()
            self.raw_data *= CC / 1e3 / self.spect_analyzer.center_freq  # convert to km/s
        elif self.rendermode == Mode.asym:
            self.raw_data = self.spect_analyzer.split_integral_vel(self.asym_sep, self.asym_width, 2)
            #self.raw_data = self.spect_analyzer.split_integral(self.asym_sep, self.asym_width, 2)
            self.raw_data = self.raw_data[..., 1] - self.raw_data[..., 0]

        if updatedisplay:
            self.update_display()

    def save_image(self):
        #output_name = tkFileDialog.asksaveasfilename(title='Image Array Filename')
        output_name = 'Image Array Filename'
        if not output_name:
            return
        self.rend.save_irender(output_name, self.raw_data,self.x,self.y)

    def save_spectra(self):
        #output_name = tkFileDialog.asksaveasfilename(title='Spectra Array Filename')
        output_name = 'Spectra Array Filename'
        if not output_name:
            return
        if self.raw_spectra is None:
            self.rend.distance_per_pixel = self.distance_per_pixel
            self.rend.stepsize = self.stepsize
            self.rend.y_pixel_offset = self.y_pixel_offset
            self.rend.x_pixel_offset = self.x_pixel_offset
            data, dfreqs, ny0, _ = self.get_il_render()
            self.raw_spectra = (noisify_spectra(data, self.noise_snr), dfreqs, ny0)
        self.rend.save_ilrender(output_name, self.raw_spectra,self.x,self.y)

    def save_range(self):
        self._saverangedialog.rend_choice = None
        self._saverangedialog.open()

    def _renderrangefromdialog(self, srd, choice):
        for i in xrange(srd.chlisbox.count()):
            item = srd.chlisbox.itemAt(i)
            widget = item.widget()
            if isinstance(widget, QtGui.QCheckBox):
                if widget.isChecked():
                    if "channel_ids" in locals():
                        channel_ids.extend([i])
                    else:
                        channel_ids = [i]

        snap_bounds = sorted((int(srd.snap_bound_init.value()), int(srd.snap_bound_end.value())))
        snap_skip = int(srd.snap_skip.value())
        snap_range = range(snap_bounds[0], snap_bounds[1], snap_skip)

        channellist = self.channellist
        save_loc = srd.savefilename

        save_loct = Template(save_loc)
        '''if len(snap_range) > 1 and '${num}' not in save_loc or len(channel_ids) > 1 and '${chan}' not in save_loc:
            print('Missing "${num}" or "${chan}" in file descriptor')
            return'''

        orig_mode, orig_snap, orig_channel = self.rendermode, self.snap, self.channel
        # if spectra is chosen, choose mode that caches spectra
        if choice == 'il':
            self.rendermode = Mode.doppler_shift

        for snap in snap_range:
            self.snap = snap
            for channel_id in channel_ids:
                self.channel = channel_id
                save_file = save_loc+str(snap)+channellist[channel_id]
                self.update(False)
                if choice == 'il':
                    self.rend.save_ilrender(save_file, self.raw_spectra,self.x,self.y)
                elif choice == 'i':
                    # process spectra into raw data if necessary
                    if self.rendermode == Mode.doppler_shift:
                        self.raw_data = self.spect_analyzer.quad_regc()
                        self.raw_data *= -CC / 1e3 / self.spect_analyzer.center_freq  # convert to km/s
                    elif self.rendermode == Mode.width:
                        self.raw_data = self.spect_analyzer.fwhm()
                        self.raw_data *= CC / 1e3 / self.spect_analyzer.center_freq  # convert to km/s
                    elif self.rendermode == Mode.asym:
                        self.raw_data = self.spect_analyzer.split_integral_vel(self.asym_sep, self.asym_width, 2)
                        self.raw_data = self.raw_data[..., 1] - self.raw_data[..., 0]
                    self.rend.save_irender(save_file, self.raw_data,self.x,self.y)
                print('wrote',save_file)

        self.rendermode, self.snap, self.channel = orig_mode, orig_snap, orig_channel
        self.raw_data = self.raw_spectra = None
        self.update()

    def get_i_render(self):
        return self.rend.i_render(self.channel, self.azimuth, -self.altitude, self.axis, reverse=self.rend_reverse,gridsplit=self.gridsplit,
                                  opacity=self.rend_opacity, verbose=False)
    def get_isun_render(self):
        return self.rend.mult_i_render(self.sdomain, self.edomain, self.step, channel=self.channel,gridsplit=self.gridsplit,
                                     opacity=self.rend_opacity,
                                     curvature=self.rend_curvature,fw=None, along_x=self.rend_along_x)

    def get_il_render(self):
        return self.rend.il_render(self.channel, self.azimuth, -self.altitude, self.axis, self.rend_reverse,gridsplit=self.gridsplit, nlamb=self.nlamb,
                                   opacity=self.rend_opacity, verbose=False)

    def get_ilsun_render(self):
        return self.rend.mult_il_render(self.sdomain, self.edomain, self.step, nlamb=self.nlamb,gridsplit=self.gridsplit, channel=self.channel,
                                     opacity=self.rend_opacity,
                                     curvature=self.rend_curvature,fw=None, along_x=self.rend_along_x)
