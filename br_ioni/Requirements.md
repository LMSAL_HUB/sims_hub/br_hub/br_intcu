Requirements:

1) python 2.7 or higher 
2) numpy 
3) scipy 
4) pycuda: Important: Follow instructions in http://wiki.tiker.net/PyCuda/Installation/Mac
5) kivy 1.8 (only for using the GUI). 19/12/2013 First install 1.7.? and then upgrade with: 

cd /Applications/Kivy.app/Contents/Resources/
mv kivy kivy_stable
git clone http://github.com/kivy/kivy
cd kivy
make

Making kivy does not work with clang; use GCC.

with clang compiler you need to add option in Renderer/__init__.py as follows (line 167):

  self.mod = SourceModule(self.cuda_code, no_extern_c=True, options=['-ccbin=/usr/bin/llvm-g++-4.2'],
                                include_dirs=['/Developer/CUDA Samples/C/common/inc',
                                              os.path.dirname(os.path.abspath(__file__))])
and include_dirs has to be modified in accord with your CUDA installation.


Another option is to install Anaconda (includes scipy, numpy and pycuda). 
In this case, only kivy 1.8 needs to be installed.
