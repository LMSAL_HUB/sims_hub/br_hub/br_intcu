import os
import numpy
import setuptools
from numpy.distutils.core import setup
from numpy.distutils.extension import Extension

try:  # do we have cython?
    from Cython.Build import cythonize
    USE_CYTHON = True
except:
    USE_CYTHON = False

NAME = "br_ioni"
PACKAGES = ["renderer", "spectanalys"]
ALL_PACKAGES = []
for package in PACKAGES:
     ALL_PACKAGES.append("%s/%s" %  (NAME,package))


VERSION = "0.0"

setup(
    name=NAME,
    version=VERSION,
    description="",
    author="Juan Martinez-Sykora et al.",
    license="BSD",
    url="http://%s.readthedocs.io" % NAME,
    keywords=['astronomy', 'astrophysics', 'solar physics', 'space', 'science'],
    classifiers=[
          'Intended Audience :: Science/Research',
          'License :: OSI Approved :: BSD License',
          'Operating System :: OS Independent',
          'Programming Language :: Python :: 3',
          'Topic :: Scientific/Engineering :: Astronomy',
          'Topic :: Scientific/Engineering :: Physics'
    ],
    packages=ALL_PACKAGES,
    python_requires='>=3.0',
)
