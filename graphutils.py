import numpy as np
import matplotlib

def whitify(ax):
    for q in ax.get_children():
        if isinstance(q, matplotlib.spines.Spine):
            q.set_color('white')

    ax.tick_params(colors='white')
    ax.get_xaxis().get_offset_text().set_color('white')
    ax.get_yaxis().get_offset_text().set_color('white')

def scaleheight(arr):
    '''
    Calculates intensity vs z-pos for some output of i_render
    '''

    return np.sum(arr, 1)
