import math as m
import os
import shlex
from collections import namedtuple

import numpy as np
from scipy.io import readsav
from scipy import interpolate

import pycuda.driver as cuda
from pycuda import curandom

from helita.sim.bifrost import BifrostData, Rhoeetab, Opatab, Bifrost_units
from helita.sim.muram import MuramAtmos
from .renderer import Renderer
from .renderer import SingAxisRenderer
import pickle

units = Bifrost_units()
EE = units.ev_to_erg              # 1.602189e-12
HH = units.hplanck                # 6.626176e-27
CC = units.clight * units.cm_to_m # 2.99792458e8 m/s
CCA = CC * 1e10                   # AA/s
HCE = HH / EE * CC * 1e10
HC2 = 2 * HH * CC * 1e26

MP = units.msi_h                  # 1.67262178e-27
KB = 1.3806488e-23                # units.KBOLTZMANN.value * units.EV_TO_ERG # 1.3806488e-23

TEMAX = 1e5
RSUN = 695.5  # solar radius in megameters

Egi = namedtuple('Egi', ['ev', 'g', 'label', 'ion'])            # Einsten coeff for OOE
Trn = namedtuple('Trn', ['irad', 'jrad', 'alamb', 'a_ul', 'f']) # Coeff for OOE

with open(os.path.join(os.environ['CUDA_LIB'],'br_ioni/ioni.cu')) as kernelfile:   # Using EOS tables in Statistical equilibrium 
    SERCUDACODE = kernelfile.read() 
with open(os.path.join(os.environ['CUDA_LIB'],'br_ioni/em.cu')) as kernelfile:     # Using EOS tables in NEQ  
    TDICUDACODE = kernelfile.read()
with open(os.path.join(os.environ['CUDA_LIB'],'br_ioni/saioni.cu')) as kernelfile: # Without EOS tables in Statistical equilibrium 
    SASERCUDACODE = kernelfile.read()
with open(os.path.join(os.environ['CUDA_LIB'],'br_ioni/saem.cu')) as kernelfile:   # Without EOS tables in NEQ  
    SATDICUDACODE = kernelfile.read()

try:
    datapath = os.environ.get('GOFT_PATH')
except:
    datapath = os.path.join(os.environ.get('LMSAL_HUB'),'/sims_py/br_intcu/br_ioni/data/')
DEFAULT_PARAMFILE = 'oxygen-II-VII-iris'

NOISEGEN = curandom.ScrambledSobol32RandomNumberGenerator()


class EmissivityRenderer(Renderer):
    locph = prev_lambd = float('nan')
    snap = None

    ux = uy = uz = e = r = ka_table = opatab = None

    def __init__(self, cuda_code, snaprange, acont_filenames,
                 name_template, data_dir='./', snap=None, cstagop=True, zcutoff=0, _DataClass=BifrostData):
        '''
        Creates an emissivity renderer.
        cuda_code is the string representation of the cuda kernel.
        Snaprange is the range of allowable snaps, represented as a tuple,
        e.g. (100, 199).
        acont_filenames is the location of the CHIANTI tables.
        name_template is the base name of the snap/aux files,
        e.g. qsmag_by00it%03
        (where %03 is a placeholder for the snap number).
        '''
        Renderer.__init__(self, cuda_code)
        self.data_dir = data_dir
        self.template = name_template
        self.snap_range = snaprange
        self.cstagop = cstagop
        if snap is None:
            snap = self.snap_range[0]
        self.acont_filenames = acont_filenames
        self.zcutoff=zcutoff
        self.DataClass=_DataClass
        ''' # NOEOS
        self.rhoeetab = Rhoeetab(fdir=data_dir)

        self.nrhobin = self.rhoeetab.params['nrhobin']
        self.dmin = m.log(self.rhoeetab.params['rhomin'])
        self.drange = m.log(self.rhoeetab.params['rhomax']) - self.dmin

        self.neibin = self.rhoeetab.params['neibin']
        self.emin = m.log(self.rhoeetab.params['eimin'])
        self.erange = m.log(self.rhoeetab.params['eimax']) - self.emin

        self.tg_table = self.rhoeetab.get_table('tg')
        self.ne_table = self.rhoeetab.get_table('ne')
        '''

        self.set_snap(snap)

    def i_render(self, channel, azimuth, altitude, axis, reverse, tau=None,
                 opacity=False, verbose=True, fw=None, stepsize=0.001):
        '''
        Calculates the total intensity of light from a particular POV.

        Channel indicates which emission spectra to look at.
        Azimuth, altitude indicate POV.
        If opacity is True, then looks at tau to see what the
        current opacity is (tau can be left as None to have no initial opacity)

        fw allows setting a different wavelength for opacity calculations
        '''
        raise NotImplementedError('EmissivityRenderer does not define i/il_render, needs to be overridden')

    def il_render(self, channel, azimuth, altitude, axis, reverse, nlamb=121,
                  dopp_width_range=1e1, tau=None, opacity=False, dnus=None,
                  verbose=True, fw=None, stepsize=0.001):
        '''
        Calculates intensities as a function of frequency.

        Channel indicates which emission spectra to look at.
        Azimuth, altitude indicate POV.
        nlamb indicates number of frequencies to sample,
        dopp_width_range indicates the range of frequencies to sample
        range is
        (std. deviation of doppler broadening at 100,000K) * dopp_width_range

        If opacity is True, then looks at tau to see what the
        current opacity is (tau can be left as None to have no initial opacity)

        Returns a tuple of:
            Array of nlamb*ysteps*xsteps, (or some combination of x, y, zsteps)
            containing intensity data
            Array of nlamb, containing the deviation from nu_0 for each index
            in the first table
            ny0
            Array of ysteps*xsteps representing the integrated opacity
        Uses dnus if specified (list of deviations from the center frequency)
        Otherwise generates test_lambdas using nlamb and dopp_width_range
        dopp_width_range specifies the frequency range
        (frange = dopp_width_range * dopp_width at tmax)
        '''
        raise NotImplementedError('EmissivityRenderer does not define i/il_render, needs to be overridden')

    def i_rendern(self, channels, azimuth, altitude, axis, reverse,
                  opacity=False, stepsize=0.001):
        '''
        Sum of i_render for multiple channels to get total intensity
        '''
        out = 0

        for channel in channels:
            iout = self.i_render(channel, azimuth, altitude, axis, reverse,
                                 opacity=opacity, stepsize=stepsize)
            if opacity:
                iout = iout[0]
            out += iout

        return out

    def il_rendern(self, channels, azimuth, altitude, axis, reverse, nout=121,
                   dopp_width_range=1e1, opacity=False, stepsize=0.001):
        '''
        Performs multiple il_renders with different lines and sums them.
        Output is given in terms of wavelength (in angstroms) rather than
        frequency.
        '''
        lmax = 0
        lmin = float('inf')

        for channel in channels:
            dopp_width = dopp_width_range * self.ny0[channel] /\
                CC * m.sqrt(2 * KB * TEMAX / self.awgt[channel] / MP) / 2
            cmin = CCA / (self.ny0[channel] + dopp_width)
            cmax = CCA / (self.ny0[channel] - dopp_width)
            lmin = min(cmin, lmin)
            lmax = max(cmax, lmax)

        out = np.zeros(self.projection_y_size, self.projection_x_size, nout)
        test_lambdas = np.linspace(lmin, lmax, nout)

        for channel in channels:
            test_freqs = CCA / test_lambdas - self.ny0[channel]
            out += self.il_render(channel, azimuth, altitude,0, 0,
                                  dnus=test_freqs, opacity=opacity,
                                  stepsize=stepsize)[0]

        print('Finished rendering channels')

        return (out, test_lambdas)

    def set_snap(self, snap):
        '''
        Sets the timestep and loads the required data from the simulation to 
        compute the intensities. Note that uses trans2comm from helita, i.e., 
        its set in c.g.s. and vertical axis is third axis. Except that velocity is 
        converted into 
        
        Note, it will cut the convection zone. 

        Parameters
        ---------
        snap - integer 
            snapshot number

        '''
        if self.snap == snap:
            return
        self.snap = snap
        if snap > self.snap_range[1] or snap < self.snap_range[0]:
            raise ValueError('Time must be in the interval (' + str(
                             self.snap_range[0]) +
                             ', ' + str(self.snap_range[1]) + ')')
            
        if self.DataClass == BifrostData: 
            self.BifrostData = self.DataClass(self.template, fdir=self.data_dir,
                                       cstagop=self.cstagop)
        elif self.DataClass == MuramAtmos: 
            self.BifrostData = self.DataClass(template=self.template, fdir=self.data_dir,iz0=400)
        
        
        self.tg = self.BifrostData.trans2comm('tg',snap)  # NOEOS
        
        if (self.locph != self.locph):  # axes not loaded yet
            self.update_axes(self.zcutoff)

        self.ux = self.BifrostData.trans2comm('ux',snap)[..., self.locph:] * units.cm_to_m
        self.dx = self.BifrostData.dx
        self.dy = self.BifrostData.dy
        self.uy = self.BifrostData.trans2comm('uy',snap)[..., self.locph:] * units.cm_to_m
        self.uz = self.BifrostData.trans2comm('uz',snap)[..., self.locph:] * units.cm_to_m
        self.r  = self.BifrostData.trans2comm('rho',snap)[..., self.locph:] # Note that this one is still in cgs 
        self.ne = self.BifrostData.trans2comm('ne',snap)[..., self.locph:]  # cgs
        self.tg = self.tg[..., self.locph:] # NOEOS


    def update_axes(self, zcut=None):
        '''
        Loads/reloads the axes, changing the z-cutoff if
        zcut is not None.
        '''
        if zcut is not None:
            self.zcutoff = zcut

        xaxis = self.BifrostData.x.astype('float32')
        yaxis = self.BifrostData.y.astype('float32')
        zaxis = self.BifrostData.z.astype('float32')
        for i in range(0,len(zaxis)):
            if zaxis[i] > self.zcutoff:
                self.locph = i
                break
        zaxis = zaxis[self.locph:]

        self.set_axes(xaxis, yaxis, zaxis)

    def save_irender(self, name, array,xx,yy):
        '''
        Saves irender output in binary format.
        File has format (int dimensions (2)), (int xsize), (int ysize),
        (data array),
        (int x-axis size), (int y-axis size), (int z-axis size),
        (x array), (y array), (z array)
        '''
        savearray(name, array)
        with open(name, mode='ab') as newfile:
            newfile.write(bytes(xx.astype('float32').data))
            newfile.write(bytes(yy.astype('float32').data))
            newfile.write(bytes(np.array((self.xaxis.size, self.yaxis.size,
                                self.zaxis.size), dtype='int32').data))
            newfile.write(bytes(self.xaxis.data))
            newfile.write(bytes(self.yaxis.data))
            newfile.write(bytes(self.zaxis.data))

    def save_ilrender(self, name, data,xx,yy):
        '''
        Saves ilrender output in binary format.
        File has format
        (int dimensions (3)), (int xsize), (int ysize), (int numfreqs),
        (data array), (freqdiff array),
        (int x-axis size), (int y-axis size), (int z-axis size),
        (x array), (y array), (z array)
        '''
        array = data[0]
        freqdiff = data[1]
        savearray(name, array)
        with open(name, mode='ab') as newfile:
            newfile.write(bytes(freqdiff.astype('float32').data))
            newfile.write(bytes(xx.astype('float32').data))
            newfile.write(bytes(yy.astype('float32').data))
            newfile.write(bytes(np.array((self.xaxis.size, self.yaxis.size,
                                self.zaxis.size), dtype='int32').data))
            newfile.write(bytes(self.xaxis.data))
            newfile.write(bytes(self.yaxis.data))
            newfile.write(bytes(self.zaxis.data))

    def channellist(self):
        '''
        Provides a list of valid channels for use in the GUI.
        '''

class StaticEmRenderer(EmissivityRenderer):
    '''
    Class for rendering emissions of a slice of the sun
    using temperature/density table lookup, assuming
    ionization in statistical equilibrium.
    '''

    def __init__(self, snaprange, name_template, acont_filenames=None,
                 data_dir='./', snap=None, cstagop=True):
        '''
        Initializes renderer, and loads data from a directory.
        Specify the allowable range of snaps as a tuple, e.g. (100, 199).
        If snap is none, picks the earliest snap specified by gpuparam.txt.
        '''
        super(StaticEmRenderer, self).__init__(SERCUDACODE, snaprange,
                                               acont_filenames,
                                               name_template, data_dir, snap)
        self.awgt = []
        self.ny0 = []
        self.cstagop=cstagop

        if acont_filenames is not None:
            self.save_accontfiles(acont_filenames)


    def save_accontfiles(self,acont_filenames):
        self.acont_tables = []

        for acontfile in acont_filenames:
            filehandler = open(acontfile, 'rb')
            ion=pickle.load(filehandler)
            self.ntgbin = len(ion.Gofnt['temperature'])
            self.nedbin = len(ion.Gofnt['eDensity'])
            self.tgmin = np.min(np.log10(ion.Gofnt['temperature']))
            self.tgrange = np.max(np.log10(ion.Gofnt['temperature'])) - self.tgmin
            self.enmin = np.log10(np.min(ion.Gofnt['eDensity']))
            self.enrange = np.log10(np.max(ion.Gofnt['eDensity'])) - self.enmin
            acont_temporal = np.transpose(ion.Gofnt['gofnt'])
            self.acont_tables.append(acont_temporal)
            self.acont_tables[-1].shape = (self.nedbin, self.ntgbin)
            self.ny0.append(CCA / ion.Gofnt['wvl'])
            self.awgt.append(ion.mass)
        self.acont_tables = np.array(self.acont_tables)
    
    def i_render(self, channel, azimuth, altitude, axis, gridsplit=20,
                reverse=False, tau=None,
                opacity=False, verbose=True, fw=None, stepsize=0.001):
        '''
        Calculates the total intensity of light from a particular POV.

        Channel indicates which emission spectra to look at.
        Azimuth, altitude indicate POV.
        If opacity is True, then looks at tau to see what the
        current opacity is (tau can be left as None to have no initial opacity)

        fw allows setting a different wavelength for opacity calculations
        Returns a tuple of an array representing intensities, and an array
        representing the integrated opacity
        '''
        tables = [('atex', self.acont_tables[channel])]
        ''', # NOEOS
                  ('entex', self.ne),
                  ('tgtex', self.tg)]'''
        
        consts = [('enmin', np.float32(self.enmin)),
                  ('enrange', np.float32(self.enrange)),
                  ('tgmin', np.float32(self.tgmin)),
                  ('tgrange', np.float32(self.tgrange))]
        
        if opacity:
            self.set_lambd(fw,channel)
            tables.append(('katex', self.ka_table))
            consts.append(('opatgmin',np.float32(self.opatab.teinit)))
            consts.append(('opatgrange',np.float32(self.opatab.dte*self.opatab.nte)))
            
        '''('dmin', np.float32(self.dmin)), #NOEOS
        ('drange', np.float32(self.drange)),
        ('emin', np.float32(self.emin)),
        ('erange', np.float32(self.erange)),
        '''

        split_tables = [('dtex', self.r),
                        ('tgtex', self.tg),
                        ('entex', self.ne)] #NOEOS

        if not opacity or tau is None:
            tau = np.zeros((self.projection_y_size, self.projection_x_size),
                            dtype='float32')
        else:
            if tau.shape != (self.projection_y_size, self.projection_x_size):
                raise Exception('Tau must have shape ' + str(
                                self.projection_y_size) +
                                ' by ' + str(self.projection_x_size))
            tau = tau.astype('float32')

        tempout = np.empty_like(tau)

        def ispec_render(self, blocksize, gridsize):
            frender = self.mod.get_function('iRender')

            # integrates to find the emission
            frender(cuda.Out(tempout), cuda.InOut(tau), np.int8(opacity),
                    block=(int(blocksize), 1, 1), grid=(int(gridsize), 1, 1))
            ispec_render.datout += tempout

        ispec_render.datout = np.zeros_like(tempout)
        '''
        def render(self, azimuth, altitude, axis, reverse, gridsplit,
           consts, tables, split_tables,
           spec_render, stepsize=0.001, verbose=True):
        '''
        self.render(azimuth, altitude, 0, 0, gridsplit, consts, tables,
                    split_tables, ispec_render, stepsize, verbose)

        return (ispec_render.datout, tau)

    def il_render(self, channel, azimuth, altitude, axis, reverse,
                  nlamb=121, dopp_width_range=1e1, tau=None, opacity=False,
                  dnus=None, verbose=True, gridsplit=20, fw=None,
                  stepsize=0.001):
        '''
        Calculates intensities as a function of frequency.

        Channel indicates which emission spectra to look at.
        Azimuth, altitude indicate POV.
        nlamb indicates number of frequencies to sample,
        dopp_width_range indicates the range of frequencies to sample
        range is
        (std. deviation of doppler broadening at 100,000K) * dopp_width_range

        If opacity is True, then looks at tau to see what the
        current opacity is (tau can be left as None to have no initial opacity)

        Returns a tuple of:
            Array of nlamb*ysteps*xsteps, (or some combination of x, y, zsteps)
             containing intensity data
            Array of nlamb, containing the deviation from nu_0 for each index
             in the first table
            nu_0
            Array of ysteps*xsteps containing integrated opacity
        Uses dnus if specified (list of deviations from the center frequency)
        Otherwise generates test_lambdas using nlamb and dopp_width_range
        dopp_width_range specifies the frequency range
        (frange = dopp_width_range * dopp_width at tmax)
        '''
        dopp_width0 = self.ny0[channel] / CC * m.sqrt(
                                            2 * KB / self.awgt[channel] / MP)

        dny = dopp_width_range * dopp_width0 * m.sqrt(TEMAX) / nlamb
        if dnus is None:
            dnus = np.empty(nlamb, dtype='float32')
            for i in range(0,nlamb):
                dnus[i] = (i - (nlamb - 1) / 2) * dny
        else:
            dnus = np.array(dnus, dtype='float32')

        tables = [('atex', self.acont_tables[channel])]
        ''',#NOEOS
                  ('entex', self.ne_table),
                  ('tgtex', self.tg_table)]'''
        '''('dmin', np.float32(self.dmin)),
          ('drange', np.float32(self.drange)),
          ('emin', np.float32(self.emin)),
          ('erange', np.float32(self.erange)),
        '''#NOEOS
        consts = [('enmin', np.float32(self.enmin)),
                  ('enrange', np.float32(self.enrange)),
                  ('tgmin', np.float32(self.tgmin)),
                  ('tgrange', np.float32(self.tgrange))]

        split_tables = [('dtex', self.r),
                        ('tgtex', self.tg),#NOEOS
                        ('uxtex', self.ux),
                        ('uytex', self.uy),
                        ('uztex', self.uz),
                        ('entex', self.ne)]#NOEOS

        if opacity:
            self.set_lambd(fw,channel)
            split_tables.append(('katex', self.ka_table)) #NOEOS

        if not opacity or tau is None:
            tau = np.zeros((self.projection_y_size, self.projection_x_size),
                            dtype='float32')
        else:
            if tau.shape != (self.projection_y_size, self.projection_x_size):
                raise Exception('Tau must have shape ' + str(
                                self.projection_y_size) +
                                ' by ' + str(self.projection_x_size))
            tau = tau.astype('float32')

        tempout2 = np.empty_like(np.empty(tau.shape + (dnus.size,),
                                 dtype='float32'))
        tempout = np.empty_like(tau)
        # scope hackery, we're not in python 3
        def ilspec_render(self, blocksize, gridsize):
            frender = self.mod.get_function('ilRender')
            for nfreq in range(0,dnus.size):
                frender(cuda.Out(tempout), cuda.In(dnus), cuda.InOut(tau),
                        np.float32(self.ny0[channel]), np.float32(dopp_width0),
                        np.int32(dnus.size), np.int8(opacity),np.int32(nfreq),
                        block=(blocksize, 1, 1), grid=(gridsize, 1, 1))

                tempout2[:,:,nfreq]+= tempout
            ilspec_render.datout= tempout2

        #ilspec_render.datout = np.zeros_like(tempout2)

        self.render(azimuth, altitude, 0, 0, gridsplit, consts, tables,
                    split_tables, ilspec_render, stepsize, verbose)

        return (ilspec_render.datout, dnus, self.ny0[channel], tau)

    def channellist(self):
        return self.acont_filenames

    def set_lambd(self, lambd, channel):
        '''
        Set wavelength for calculating opacity table as a function of temperature.
        from anzer & heinzel apj 622: 714-721, 2005, march 20
        these clowns have a couple of great big typos in their reported c's.... correct values to 
        be found in rumph et al 1994 aj, 107: 2108, june 1994

        gaunt factors are set to 0.99 for h and 0.85 for heii, which should be good enough
        for the purposes of this code
        
        Parameters
        ----------
        lambda - real 
            wavelenght in AA 
        channel - integer 
            select the channel if a list is provided. 
        '''
        if self.opatab is None:
            self.opatab = Opatab(fdir=datapath)
        if lambd is None:
            lambd = CCA / self.ny0[channel]
        if lambd != self.prev_lambd:
            self.prev_lambd = lambd
        self.ka_table = self.opatab.h_he_absorb(lambd)
        
        
    def load_m2ratio(self, filename='fe_10_257.259_mit.opy'):
        '''
        Reads save file from Enrico Landi for MIT effect.

        Parameters
        ----------
        filenme - integer 
        '''
        filehandler = open(filename, 'rb')
        ion=pickle.load(filehandler)

        self.tgmin = np.min(np.log10(ion.Gofnt['temperature']))
        self.tgrange = np.max(np.log10(ion.Gofnt['temperature'])) - self.tgmin
        self.enmin = np.log10(np.min(ion.Gofnt['eDensity']))
        self.enrange = np.log10(np.max(ion.Gofnt['eDensity'])) - self.enmin
        
        self.m2tex = ion.Gofnt['gofnt']
        self.m2tex[np.where(ion.Gofnt['gofnt'] < 0)] = 0
        self.modbmin = np.min(ion.Gofnt['b_gauss'])
        self.modbrange = np.max(ion.Gofnt['b_gauss']) - self.modbmin
        

    def mult_i_render(self, sdomain, edomain, step, nlamb=121, channel=0,
                      opacity=False, curvature=True, fw=False, along_x=False,
                      gridsplit=20, stepsize=0.001):
        '''
        Uses a renderer to render a domain multiple times, and stack it
        on top of itself repeatedly.
        [sdomain, edomain] is the range of timestamps to use
        '''
        tau=None
        emiss = 0
        altitude = 0

        self.x_pixel_offset = self.y_pixel_offset = 0
        self.update_axes(0.0)
        mdomain = (float(sdomain) + float(edomain)) / float(2.0)
        zrange = np.ptp(self.zaxis)
        dtheta = m.atan(np.ptp(self.yaxis) / RSUN)

        for cdomain in range(sdomain, edomain + 1, step):
            self.set_snap(cdomain)

            offset = 0
            if curvature:  # tilt/shift
                altitude = dtheta * (mdomain - cdomain) / step
                offset = (zrange / 2 + RSUN) * (m.cos(altitude) - 1)
                self.y_pixel_offset = offset / self.distance_per_pixel
                print("Rendering time " + str(cdomain) + " with altitude " +
                       str(m.degrees(altitude)) + " offset " + str(offset),end="\r",flush=True)

            output = self.i_render(channel, 0 if along_x else 90, altitude, 0,
                                   0, gridsplit=gridsplit, tau=tau,
                                   opacity=opacity, fw=fw, stepsize=stepsize)
            #            if opacity:
            demiss, tau = output
            # else:
            #    demiss = output

            emiss += demiss

        return (emiss,tau)

    def mult_il_render(self, sdomain, edomain, step, nlamb=121, channel=0,
                       gridsplit=20, opacity=False, curvature=True,
                       fw=False, along_x=False, stepsize=0.001):
        '''
        Uses a renderer to render a domain multiple times, and stack it on
        top of itself repeatedly.
        [sdomain, edomain] is the range of timestamps to use
        '''
        tau=None
        emiss = 0
        altitude = 0

        self.x_pixel_offset = self.y_pixel_offset = 0
        self.update_axes(0.0)

        mdomain = (float(sdomain) + float(edomain)) / float(2.0)
        zrange = np.ptp(self.zaxis)
        dtheta = m.atan(np.ptp(self.yaxis) / RSUN)

        for cdomain in range(sdomain, edomain + 1, step):
            self.set_snap(cdomain)

            offset = 0
            if curvature:  # tilt/shift
                altitude = dtheta * (mdomain - cdomain) / step
                offset = (zrange / 2 + RSUN) * (m.cos(altitude) - 1)
                self.y_pixel_offset = offset / self.distance_per_pixel
                print("Rendering time " + str(cdomain) + " with altitude " +
                       str(m.degrees(altitude)) + " offset " + str(offset),end="\r",flush=True)

            output = self.il_render(channel, 0 if along_x else 90, altitude, 0,
                                    0, gridsplit=gridsplit, nlamb=nlamb,
                                    tau=tau, opacity=opacity, fw=fw,
                                    stepsize=stepsize)
            # if opacity:
            demiss, dnus, ny0, tau = output
            # else:
            #     demiss, _ = output

            emiss += demiss
        return (emiss, dnus, ny0, tau)

class TDIEmRenderer(EmissivityRenderer):
    '''
    Class for rendering emissions of Bifrost numerical model
    using calculated ionization OUT OF EQUILIBRIUM for any angle.
    '''
    #cache emissivity for speed when rendering
    level = -1
    em = None

    def __init__(self, snaprange, acont_filenames, name_template,
                 data_dir='./', snap=None,
                 paramfile='oxygen-II-VII-iris', cstagop=True):
        '''
        Initializes renderer, and loads data from a directory and paramfile.
        If snap is none, picks the earliest snap specified by gpuparam.txt.
        '''
        super(TDIEmRenderer, self).__init__(TDICUDACODE, snaprange,
                                            acont_filenames, name_template,
                                            data_dir, snap,cstagop)

        self.egis = []  # array of (ev, g, ion)
        self.trns = []

        with open(data_dir + '/' + paramfile) as tdiparamf:
            data = [line for line in tdiparamf.readlines() if line[0] != '*']

        #parse the tdiparamfile
        self.element_name = data.pop(0).strip()
        self.ab, self.awgt = (float(i) for i in data.pop(0).split())
        nk, nlines, _, _ = (int(i) for i in data.pop(0).split())

        for _ in range(0,nk):
            datstring = shlex.split(data.pop(0))
            ev = float(datstring[0]) * CC * 1e2 * HH / EE
            g = float(datstring[1])
            label = datstring[2]
            ion = int(datstring[3])
            self.egis.append(Egi(ev, g, label, ion))

        for _ in range(0,nlines):
            j, i, f, _, _, _, _, _, _, _ = (
                                    float(i) for i in data.pop(0).split())
            j = int(j)
            i = int(i)

            dn, up = i, j
            if self.egis[j - 1].ev < self.egis[i - 1].ev:
                dn, up = j, i
            irad = dn - 1
            jrad = up - 1

            alamb = HCE / (self.egis[jrad].ev - self.egis[irad].ev)
            a_ul = f * 6.6702e15 * self.egis[irad].g / (
                                                self.egis[jrad].g * alamb ** 2)

            self.trns.append(Trn(irad, jrad, alamb, a_ul, f))

    def i_render(self, level, azimuth, altitude, axis, reverse,
                 tau=None, opacity=False, verbose=True, gridsplit=20, fw=None,
                 stepsize=0.001):
        '''
        Calculates the total intensity of light from a particular POV.

        Channel indicates which emission spectra to look at.
        Azimuth, altitude indicate POV.
        If opacity is True, then looks at tau to see what the
        current opacity is (tau can be left as None to have no initial opacity)

        fw allows setting a different wavelength for opacity calculations
        Returns a tuple of an array representing intensities, and an array
        representing the integrated opacity
        '''
        if level != self.level:
            self.em = self.get_emissivities(level)
            self.level = level

        consts = []
        tables = []
        split_tables = [('emtex', self.em)]

        if opacity:
            self.set_lambd(fw,level)
            tables.append(('katex', self.ka_table))
            consts.append(('opatgmin',np.float32(self.opatab.teinit)))
            consts.append(('opatgrange',np.float32(self.opatab.dte*self.opatab.nte)))
            split_tables.extend([('dtex', self.r),
                                ('eetex', self.e)])
            consts.extend([('dmin', np.float32(self.dmin)),
                           ('drange', np.float32(self.drange)),
                           ('emin', np.float32(self.emin)),
                           ('erange', np.float32(self.erange))])

        if not opacity or tau is None:
            tau = np.zeros((self.projection_y_size, self.projection_x_size),
                           dtype='float32')
        else:
            if tau.shape != (self.projection_y_size, self.projection_x_size):
                raise Exception('Tau must have shape ' + str(
                                self.projection_y_size) +
                                ' by ' + str(self.projection_x_size))
            tau = tau.astype('float32')

        tempout = np.empty_like(tau)

        def ispec_render(self, blocksize, gridsize):
            frender = self.mod.get_function('iRender')
            frender(cuda.Out(tempout), cuda.InOut(tau), np.int8(opacity),
                    block=(int(blocksize), 1, 1), grid=(int(gridsize), 1, 1))
            ispec_render.datout += tempout

        ispec_render.datout = np.zeros_like(tempout)

        self.render(azimuth, altitude, 0, 0, gridsplit, consts, tables,
                    split_tables, ispec_render, stepsize, verbose)
        '''
        def render(self, azimuth, altitude, axis, reverse, gridsplit,
           consts, tables, split_tables,
           spec_render, stepsize=0.001, verbose=True):
        '''
        return (ispec_render.datout, tau)

    def il_render(self, level, azimuth, altitude, axis, reverse,
                  nlamb=121, dopp_width_range=1e1, tau=None, opacity=False,
                  dnus=None, verbose=True, gridsplit=20, fw=None,
                  stepsize=0.001):
        '''
        Calculates intensities as a function of frequency.

        Channel indicates which emission spectra to look at.
        Azimuth, altitude indicate POV.
        nlamb indicates number of frequencies to sample,
        dopp_width_range indicates the range of frequencies to sample range
        is
        (std. deviation of doppler broadening at 100,000K) * dopp_width_range

        If opacity is True, then looks at tau to see what the
        current opacity is (tau can be left as None to have no initial opacity)

        Returns a tuple of:
            Array of nlamb*ysteps*xsteps, (or some combination of x, y, zsteps)
            containing intensity data
            Array of nlamb, containing the deviation from nu_0 for each index
            in the first table
            nu_0
            Array of ysteps*xsteps containing integrated opacity
        Uses dnus if specified (list of deviations from the center frequency)
        Otherwise generates test_lambdas using nlamb and dopp_width_range
        dopp_width_range specifies the frequency range
        (frange = dopp_width_range * dopp_width at tmax)
        '''
        if level != self.level:
            self.em = self.get_emissivities(level)
            self.level = level

        ny0 = CCA / self.trns[level].alamb
        dopp_width0 = ny0 / CC * m.sqrt(2 * KB / self.awgt / MP)

        dny = dopp_width_range * dopp_width0 * m.sqrt(TEMAX) / nlamb
        if dnus is None:
            dnus = np.empty(nlamb, dtype='float32')
            for i in range(0,nlamb):
                dnus[i] = (i - (nlamb - 1) / 2) * dny
        else:
            dnus = np.array(dnus, dtype='float32')

        consts = [('dmin', np.float32(self.dmin)),
                  ('drange', np.float32(self.drange)),
                  ('emin', np.float32(self.emin)),
                  ('erange', np.float32(self.erange))]
        #tables = [('tgtex', self.tg_table)] #NOEOS
        split_tables = [('emtex', self.em),
                        ('uztex', self.uz),
                        ('uytex', self.uy),
                        ('uxtex', self.ux),
                        ('tgtex', self.tg),
                        ('dtex', self.r)]

        if opacity:
            self.set_lambd(fw,level)
            tables.append(('katex', self.ka_table))
            consts.append(('opatgmin',np.float32(self.opatab.teinit)))
            consts.append(('opatgrange',np.float32(self.opatab.dte*self.opatab.nte)))
            
        reset_tau = False
        if not opacity or tau is None:
            tau = np.zeros((self.projection_y_size, self.projection_x_size),
                            dtype='float32')
            reset_tau = True
        else:
            if tau.shape != (self.projection_y_size, self.projection_x_size):
                raise Exception('Tau must have shape ' + str(
                                self.projection_y_size) +
                                ' by ' + str(self.projection_x_size))
            tau = tau.astype('float32')


        tempout2 = np.empty_like(np.empty(tau.shape + (dnus.size,),
                                 dtype='float32'))
        tempout = np.empty_like(tau)
        # scope hackery, we're not in python 3
        def ilspec_render(self, blocksize, gridsize):
            frender = self.mod.get_function('ilRender')
            for nfreq in range(0,dnus.size):
                if (reset_tau): 
                    tau = np.zeros((self.projection_y_size, self.projection_x_size),
                            dtype='float32')
                frender(cuda.Out(tempout), cuda.In(dnus), cuda.InOut(tau),
                        np.float32(self.ny0[channel]), np.float32(dopp_width0),
                        np.int32(dnus.size), np.int8(opacity), np.int32(nfreq),
                        block=(blocksize, 1, 1), grid=(gridsize, 1, 1))
                '''
                ilRender(float *out, float *dnus, float *tau,
                float nu0, float dopp_width0, int nlamb, bool opacity,
                int nfreq)
                '''
                tempout2[:,:,nfreq]+= tempout
            ilspec_render.datout= tempout2

        #ilspec_render.datout = np.zeros_like(tempout2)

        self.render(azimuth, altitude, 0, 0, gridsplit, consts, tables,
                    split_tables, ilspec_render, stepsize, verbose)
        '''
        def render(self, azimuth, altitude, axis, reverse, gridsplit,
           consts, tables, split_tables,
           spec_render, stepsize=0.001, verbose=True):
        '''
        return (ilspec_render.datout, dnus, ny0, tau)

    def mult_i_render(self, sdomain, edomain, step, nlamb=121, channel=0,
                      opacity=False, curvature=True, fw=False, along_x=False,
                      gridsplit=20, stepsize=0.001):
        '''
        Uses a renderer to render a domain multiple times, and stack it
        on top of itself repeatedly.
        [sdomain, edomain] is the range of timestamps to use
        '''
        tau=None
        emiss = 0
        altitude = 0

        self.x_pixel_offset = self.y_pixel_offset = 0
        self.update_axes(0.0)
        mdomain = (float(sdomain) + float(edomain)) / float(2.0)
        zrange = np.ptp(self.zaxis)
        dtheta = m.atan(np.ptp(self.yaxis) / RSUN)


        for cdomain in range(sdomain, edomain + 1, step):
            self.set_snap(cdomain)

            offset = 0
            if curvature:  # tilt/shift
                altitude = dtheta * (mdomain - cdomain) / step
                offset = (zrange / 2 + RSUN) * (m.cos(altitude) - 1)
                self.y_pixel_offset = offset / self.distance_per_pixel
                print("Rendering time " + str(cdomain) + " with altitude " +
                       str(m.degrees(altitude)) + " offset " + str(offset),end="\r",flush=True)

            output = self.i_render(channel, 0 if along_x else 90, altitude, 0,
                                   0, gridsplit=gridsplit, tau=tau,
                                   opacity=opacity, fw=fw, stepsize=stepsize)
            #            if opacity:
            demiss, tau = output
            # else:
            #    demiss = output

            emiss += demiss

        return (emiss,tau)

    def mult_il_render(self, sdomain, edomain, step, nlamb=121, channel=0,
                       opacity=False, curvature=True, fw=False, along_x=False,
                       gridsplit=20, stepsize=0.001):
        '''
        Uses a renderer to render a domain multiple times, and stack it on
        top of itself repeatedly.
        [sdomain, edomain] is the range of timestamps to use
        '''
        tau=None
        emiss = 0
        altitude = 0

        self.x_pixel_offset = self.y_pixel_offset = 0
        self.update_axes(0.0)

        mdomain = (float(sdomain) + float(edomain)) / float(2.0)
        zrange = np.ptp(self.zaxis)
        dtheta = m.atan(np.ptp(self.yaxis) / RSUN)


        for cdomain in range(sdomain, edomain + 1, step):
            self.set_snap(cdomain)

            offset = 0
            if curvature:  # tilt/shift
                altitude = dtheta * (mdomain - cdomain) / step
                offset = (zrange / 2 + RSUN) * (m.cos(altitude) - 1)
                self.y_pixel_offset = offset / self.distance_per_pixel
                print("Rendering time " + str(cdomain) + " with altitude " +
                       str(m.degrees(altitude)) + " offset " + str(offset),end="\r",flush=True)

            output = self.il_render(channel, 0 if along_x else 90, altitude, 0,
                                    0, gridsplit=gridsplit, nlamb=nlamb,
                                    tau=tau, opacity=opacity, fw=fw,
                                    stepsize=stepsize)
            # if opacity:
            demiss, dnus, ny0, tau = output
            # else:
            #     demiss, _ = output

            emiss += demiss
        return (emiss, dnus, ny0, tau)

    def get_emissivities(self, level=None):
        '''
        Returns a 3d table giving the emissivity at each point.
        If level is None, prompts will guide you through selecting
        a level.
        '''
        if (level is None):
            tion = int(input('Input the oxygen ionization level to check: '))
            ionln = [i for i, v in enumerate(self.egis) if v.ion == tion]

            print('These are the ' + self.element_name + str(tion) + ' lines:')
            print('nr\tlvl l\tlvl u\tname lvl l\tname lvl u\twavelength\tEinstein Aij\tion')
            print('--------------------------------------------------------------------------------')

            for n, t in enumerate(self.trns):
                for ind in ionln:
                    if t.jrad == ind or t.irad == ind:
                        print(str(n) + ')\t' + str(t.irad + 1) + '\t' +
                              str(t.jrad + 1) + '\t' +
                              self.egis[t.irad].label + '\t' +
                              self.egis[t.jrad].label + '\t' +
                              '%e' % t.alamb + '\t' + '%e' % t.a_ul +
                              '\t' + str(self.egis[t.irad].ion))
                        break

            level = int(input('Choose a line: '))

        tline = self.trns[level]
        ion_densities = self.BifrostData.getooevar(tline.jrad)
        return ((HH * CC / (tline.alamb * 1e-10) * tline.a_ul / (
                                4 * m.pi)) * ion_densities)[..., :self.locph]

    def channellist(self):
        return [self.egis[t.irad].label + " -->" + self.egis[t.jrad].label for t in self.trns]

    def set_lambd(self, lambd, level):
        '''
        Set wavelength for calculating opacity table as a function of temperature.
        from anzer & heinzel apj 622: 714-721, 2005, march 20
        these clowns have a couple of great big typos in their reported c's.... correct values to 
        be found in rumph et al 1994 aj, 107: 2108, june 1994

        gaunt factors are set to 0.99 for h and 0.85 for heii, which should be good enough
        for the purposes of this code
        
        Parameters
        ----------
        lambda - real 
            wavelenght in AA 
        channel - integer 
            select the channel if a list is provided. 
        '''
        if self.opatab is None:
            self.opatab = Opatab(fdir=datapath)
        if lambd is None:
            lambd = self.trns[level].alamb
        if lambd != self.prev_lambd:
            self.prev_lambd = lambd
        self.ka_table = self.opatab.h_he_absorb(lambd)
        

    def load_m2ratio(self, filename='fe_10_257.259_mit.opy',iwvl=2830):
        '''
        Reads save file from Enrico Landi for MIT effect.

        
        Parameters
        ----------
        filenme - integer 
        '''
        
        filehandler = open(filename, 'rb')
        ion=pickle.load(filehandler)

        self.tgmin = np.min(np.log10(ion.Gofnt['temperature']))
        self.tgrange = np.max(np.log10(ion.Gofnt['temperature'])) - self.tgmin
        self.enmin = np.log10(np.min(ion.Gofnt['eDensity']))
        self.enrange = np.log10(np.max(ion.Gofnt['eDensity'])) - self.enmin
        
        self.m2tex = ion.Gofnt['gofnt']
        self.m2tex[np.where(ion.Gofnt['gofnt'] < 0)] = 0
        self.modbmin = np.min(ion.Gofnt['b_gauss'])
        self.modbrange = np.max(ion.Gofnt['b_gauss']) - self.modbmin
        
        
class SAEmissivityRenderer(SingAxisRenderer):
    locph = prev_lambd = float('nan')
    snap = None

    uu = e = r = ka_table = opatab = None
    #JMS: working here
    #def __init__(self, cuda_code, data_dir=DEFAULT_LOC, snap=None):
    #    super(SAEmissivityRenderer, self).__init__(cuda_code)


    def __init__(self, cuda_code, snaprange, acont_filenames,
                 name_template, axis=2, data_dir='./', snap=None,
                 do_mit=None, cstagop=True, zcutoff=0, 
                 _DataClass=BifrostData, **kwargs):

        SingAxisRenderer.__init__(self, cuda_code)
        self.data_dir = data_dir
        self.template = name_template
        self.snap_range = snaprange
        self.cstagop = cstagop
        if snap is None:
            snap = self.snap_range[0]
        self.acont_filenames = acont_filenames
        self.axis=axis
        self.zcutoff = zcutoff
        self.DataClass = _DataClass
        self.set_snap(snap, do_mit=do_mit)
        
        
    def i_rendern(self, channel, azimuth, altitude, axis, reverse,
                  opacity=False, gridsplit=20, stepsize = 0.001):
        '''
        Sum of i_render for multiple channels to get total intensity
        '''
        out = 0

        for channel in channels:
            iout = self.i_render(channel, azimuth, altitude, axis, reverse,
                                 opacity=opacity, gridsplit=gridsplit,
                                 stepsize=stepsize)
            if opacity:
                iout = iout[0]
            out += iout

        return out

    
    def il_rendern(self, channels, azimuth, altitude, axis, reverse,
                   gridsplit=10, nout=121, dopp_width_range=1e1,
                   opacity=False, stepsize=0.001):
        '''
        Performs multiple il_renders with different lines and sums them.
        Output is given in terms of wavelength (in angstroms) rather than
        frequency.
        '''
        lmax = 0
        lmin = float('inf')

        for channel in channels:
            dopp_width = dopp_width_range * self.ny0[channel] /\
                CC * 1e2 * m.sqrt(2 * KB * TEMAX / self.awgt[channel] / MP) / 2
            cmin = CCA / (self.ny0[channel] + dopp_width)
            cmax = CCA / (self.ny0[channel] - dopp_width)
            lmin = min(cmin, lmin)
            lmax = max(cmax, lmax)

        out = np.zeros(self.projection_x_size, self.projection_y_size, nout)
        test_lambdas = np.linspace(lmin, lmax, nout)

        for channel in channels:
            test_freqs = CCA / test_lambdas - self.ny0[channel]
            out += self.il_render(channel, azimuth, altitude, axis, reverse,
                                  dnus=test_freqs, opacity=opacity,
                                  gridsplit=gridsplit, stepsize=stepsize)[0]

        print('Finished rendering channels')

        return (out, test_lambdas)

    
    def set_snap(self, snap, do_mit=None):
        '''
        Sets the timestep and loads the required data from the simulation to 
        compute the intensities. Note that uses trans2comm from helita, i.e., 
        its set in c.g.s. and vertical axis is third axis. Except that velocity is 
        converted into 

        Note, it will cut the convection zone. 
            
        Parameters
        ---------
        snap - integer 
            snapshot number
        do_mit - string (Default None)
            will read the magnetic field strength in cgs to take into account the MIT effect. 
        '''

        if self.snap == snap:
            return
        self.snap = snap
        if snap > self.snap_range[1] or snap < self.snap_range[0]:
            raise ValueError('Time must be in the interval (' + str(
                              self.snap_range[0]) +
                             ', ' + str(self.snap_range[1]) + ')')

        if self.DataClass == BifrostData: 
            print('BifrostData')
            self.BifrostData = self.DataClass(self.template,
                                       fdir=self.data_dir,
                                       cstagop=self.cstagop,
                                       snap=snap)
        elif self.DataClass == MuramAtmos: 
            print('Muram')
            self.BifrostData = self.DataClass(template=self.template, fdir=self.data_dir,iz0=400)
        else: 
            print('No data')
            
        self.tg = self.BifrostData.trans2comm('tg',self.snap)
        
        if (self.locph != self.locph):  # axes not loaded yet
            self.update_axes(self.zcutoff)

        self.tg = self.tg[..., self.locph:]
        self.r = self.BifrostData.trans2comm('rho',self.snap)[..., self.locph:] # Note that this one is still in cgs 
        self.ne = self.BifrostData.trans2comm('ne',self.snap)[..., self.locph:] # cgs

        self.dx = self.BifrostData.dx
        self.dy = self.BifrostData.dy

        if self.axis == 0:
            self.uu = self.BifrostData.trans2comm('ux',self.snap)[..., self.locph:] * units.cm_to_m
        elif self.axis == 1:
            self.uu = self.BifrostData.trans2comm('uy',self.snap)[..., self.locph:] * units.cm_to_m
        else:
            self.uu = self.BifrostData.trans2comm('uz',self.snap)[..., self.locph:] * units.cm_to_m
        
        if do_mit is not None:
            try: 
                self.modb = self.BifrostData.trans2comm('modb',self.snap)[..., self.locph:]
            except: 
                self.modb = np.sqrt(self.BifrostData.trans2comm('bx',self.snap)[..., self.locph:]**2 + 
                            self.BifrostData.trans2comm('by',self.snap)[..., self.locph:]**2 + 
                            self.BifrostData.trans2comm('bz',self.snap)[..., self.locph:]**2)

    def update_axes(self, zcut=None):
        '''
        Loads/reloads the axes, changing the z-cutoff if
        zcut is not None.
        '''
        if zcut is not None:
            self.zcutoff = zcut

        xaxis = self.BifrostData.x.astype('float32')
        yaxis = self.BifrostData.y.astype('float32')
        zaxis = self.BifrostData.z.astype('float32')
        for i in range(0,len(zaxis)):
            if zaxis[i] > self.zcutoff:
                self.locph = i
                break
        zaxis = zaxis[self.locph:]
        self.set_axes(xaxis, yaxis, zaxis)

    def save_irender(self, name, array,xx,yy):
        '''
        Saves irender output in binary format.
        File has format (int dimensions (2)), (int xsize), (int ysize),
        (data array),
        (int x-axis size), (int y-axis size), (int z-axis size),
        (x array), (y array), (z array)
        '''
        savearray(name, array)
        with open(name, mode='ab') as newfile:
            newfile.write(bytes(xx.astype('float32').data))
            newfile.write(bytes(yy.astype('float32').data))
            newfile.write(bytes(np.array((self.xaxis.size, self.yaxis.size,
                                self.zaxis.size), dtype='int32').data))
            newfile.write(bytes(self.xaxis.data))
            newfile.write(bytes(self.yaxis.data))
            newfile.write(bytes(self.zaxis.data))

    def save_ilrender(self, name, data,xx,yy):
        '''
        Saves ilrender output in binary format.
        File has format (int dimensions (3)), (int xsize), (int ysize),
        (int numfreqs), (data array), (freqdiff array),
        (int x-axis size), (int y-axis size), (int z-axis size),
        (x array), (y array), (z array)
        '''
        array = data[0]
        freqdiff = data[1]
        savearray(name, array)
        with open(name, mode='ab') as newfile:
            newfile.write(bytes(freqdiff.astype('float32').data))
            newfile.write(bytes(xx.astype('float32').data))
            newfile.write(bytes(yy.astype('float32').data))
            newfile.write(bytes(np.array((self.xaxis.size, self.yaxis.size,
                                self.zaxis.size), dtype='int32').data))
            newfile.write(bytes(self.xaxis.data))
            newfile.write(bytes(self.yaxis.data))
            newfile.write(bytes(self.zaxis.data))

    def channellist(self):
        '''
        Provides a list of valid channels for use in the GUI.
        '''

    def i_render(self, channel, azimuth, altitude, axis, reverse,
                 tau=None, opacity=False, verbose=True, fw=None,
                 gridsplit=20, stepsize=0.001, do_mit=None):
        '''
        Calculates the total intensity of light from a particular POV I SI.
        
        Parameters
        ---------
        channel - string
            Name of the spectral line to calculate. In order to know the f
            ormat, $BIFROST/PYTHON/br_int/br_ioni/data
            contains files with the G(T,ne), usually name.opy. spline must
            be name, e.g., 'fe_8_108.073'.
        azimuth - real
            This allows to trace rays for any angle. Azimuth indicate POV.
        altitude - real
            This allows to trace rays for any angle. Altitude indicate POV.
        axis - integer number: 0 = x, 1 = y, 2 = z
            allows to chose the LOS integration axis (see azimuth and altitude)
        reverse - logical (False)
            Will integrate forward or backwards along the LOS. 
        tau - 2D array (None)
            Initial tau. Default (None), means it is set to zero. 
        opacity - logical (False)
            Calcuate opacities. Tau can be left as None to have no initial opacity.
        verbose - logical (False)
            provides some print outs.
        gridsplit - integer (20)
            number of grid points along the LOS that will run the CUDA code at once. 
        fw -  real (None)
            allows setting a different wavelength for opacity calculations used for set_lambd function 
        stepsize - real (0.001)
            grid spacing in the CUDA code.
        do_mit - string (Default None)
            MIT effect (work in progress)

        Returns 
        ---------
        a tuple of:
            Array of ysteps*xsteps, (or some combination of x, y, zsteps)
            containing intensity data
            
        '''
        raise NotImplementedError('EmissivityRenderer does not define i/il_render, needs to be overridden')

        
    def il_render(self, channel, azimuth, altitude, axis, reverse, nlamb=121,
                  dopp_width_range=1e1, tau=None, opacity=False, dnus=None,
                  verbose=True, gridsplit=20, fw=None, stepsize=0.001, do_mit=None):
        '''
        Calculates intensities as a function of frequency.

        Parameters
        ---------
        channel - string
            Name of the spectral line to calculate. In order to know the f
            ormat, $BIFROST/PYTHON/br_int/br_ioni/data
            contains files with the G(T,ne), usually name.opy. spline must
            be name, e.g., 'fe_8_108.073'.
        azimuth - real
            This allows to trace rays for any angle. Azimuth indicate POV.
        altitude - real
            This allows to trace rays for any angle. Altitude indicate POV.
        axis - integer number: 0 = x, 1 = y, 2 = z
            allows to chose the LOS integration axis (see azimuth and altitude)
        reverse - logical (False)
            Will integrate forward or backwards along the LOS. 
        nlamb - integer (121)
            indicates number of frequencies to sample,
        dopp_width_range - real (10.)
            sets the spectral pixel size km/s. 
            (std. deviation of doppler broadening at 100,000K) * dopp_width_range
        tau - 2D array (None)
            Initial tau. Default (None), means it is set to zero. 
        opacity - logical (False)
            Calcuate opacities. Tau can be left as None to have no initial opacity.
        dnus - array (None)
            input spectra array in km/s.
        verbose - logical (False)
            provides some print outs.
        gridsplit - integer (20)
            number of grid points along the LOS that will run the CUDA code at once. 
        fw -  real (None)
            allows setting a different wavelength for opacity calculations used for set_lambd function 
        stepsize - real (0.001)
            grid spacing in the CUDA code.
        do_mit - string (Default None)
            MIT effect (work in progress)

        Returns 
        ---------
        a tuple of:
            Array of nlamb*ysteps*xsteps, (or some combination of x, y, zsteps)
            containing intensity data
            Array of nlamb, containing the deviation from nu_0 for each index
            in the first table
        Uses dnus if specified (list of deviations from the center frequency)
        Otherwise generates test_lambdas using nlamb and dopp_width_range
        dopp_width_range specifies the frequency range
        (frange = dopp_width_range * dopp_width at tmax)
        '''
        
        raise NotImplementedError('EmissivityRenderer does not define i/il_render, needs to be overridden')

        
        
class SAStaticEmRenderer(SAEmissivityRenderer):
    '''
    Class for rendering emissions without EOS tables, assuming
    ionization in statistical equilibrium along x, y or z.
    '''

    def __init__(self, snaprange, name_template, axis=2, acont_filenames=None,
                 data_dir='.', snap=None, cstagop=True, zcutoff=0, **kwargs):
        '''
        Initializes renderer, and loads data from a directory.

        Parameters
        ---------
        snaprange - list of integers
            list of snapshots
        name_template - string
            root name of the simulation 
        axis - integer (2)
            selected LOS axis 0=x, 1=y and 2=z. 
        acont_filenames - string list (None)
             files name, e.g., (['fe_10_170.575.opy'])
        data_dir - string ('.')
            simulation path
        snap - integer (None) 
            snapshot number
        cstagop - logical (True)
            will interpolate (or not) from a stagger mesh to put all points grid center. 
        zcutoff - real (0)
            cuts the convection zone. The value iz z=zcutoff in cm units. 
        do_mit - string (Default None)
            MIT effect (work in progress). 
        '''
        super(SAStaticEmRenderer, self).__init__(SASERCUDACODE, snaprange,
                                                 acont_filenames,
                                                 name_template, axis=axis, data_dir=data_dir,
                                                 snap=snap, cstagop=cstagop, zcutoff=zcutoff, **kwargs)
        self.acont_tables = []
        self.awgt = []
        self.ny0 = []
        self.axis=axis
        if acont_filenames is not None:
            self.save_accontfiles(acont_filenames)
        self.zcutoff = zcutoff

        
    def save_accontfiles(self,acont_filenames):
        '''
        Loads the G(T,ne) tables. 
        Important, this G(T,ne) include the abundance. 
        
         Parameters
        ----------
        acont_filenames - string list
            files name, e.g., (['fe_10_170.575.opy'])
        '''
        self.acont_tables = []

        for acontfile in acont_filenames:
            filehandler = open(acontfile, 'rb')
            ion=pickle.load(filehandler)
            self.ntgbin = len(ion.Gofnt['temperature'])
            self.nedbin = len(ion.Gofnt['eDensity'])
            self.tgmin = np.min(np.log10(ion.Gofnt['temperature']))
            self.tgrange = np.max(np.log10(ion.Gofnt['temperature'])) - self.tgmin
            self.enmin = np.log10(np.min(ion.Gofnt['eDensity']))
            self.enrange = np.log10(np.max(ion.Gofnt['eDensity'])) - self.enmin
            acont_temporal = np.transpose(ion.Gofnt['gofnt'])
            self.acont_tables.append(acont_temporal)
            self.acont_tables[-1].shape = (self.nedbin, self.ntgbin)
            self.ny0.append(CCA / ion.Gofnt['wvl'])
            self.awgt.append(ion.mass)
        self.acont_tables = np.array(self.acont_tables)

        
    def i_render(self, channel, azimuth, altitude, axis, reverse=False,
                 tau=None, opacity=False, verbose=True, fw=None,
                 gridsplit=20 , stepsize=0.001, do_mit=None):
        '''
        Computes the total intensity a long one of the selected axis in SE, 
        
         Parameters
        ----------
        channel - string
            Name of the spectral line to calculate. In order to know the f
            ormat, $BIFROST/PYTHON/br_int/br_ioni/data
            contains files with the G(T,ne), usually name.opy. spline must
            be name, e.g., 'fe_8_108.073'.
        azimuth - real
            Not used. This allows to trace rays for any angle. In this cases
            uses none sa modules. In this case, axis parameter is not need to be used.
        altitude - real
            Not used. This allows to trace rays for any angle. In this cases
            uses none sa modules. In this case, axis parameter is not need to be used.
        axis - integer number: 0 = x, 1 = y, 2 = z
            allows to chose the LOS integration axis (see azimuth and altitude)
        reverse - logical (False)
            Will integrate forward or backwards along the LOS. 
        tau - 2D array (None)
            Initial tau. Default (None), means it is set to zero. 
        opacity - logical (False)
            Calcuate opacities.
        verbose - logical (False)
            provides some print outs. 
        fw -  real (None)
           allows setting a different wavelength for opacity calculations used for set_lambd function 
        gridsplit - integer (20)
            number of grid points along the LOS that will run the CUDA code at once. 
        stepsize - real (0.001)
            grid spacing in the CUDA code. 
        do_mit - string (Default None)
            MIT effect (work in progress)
            
        Returns
        -------
        array - ndarray
            Intensity array with the dimensions of the 2D spatial from the simulation in SI.
        array - ndarray
            Intensity array with the dimensions of the 2D spatial from the simulation in cgs.
        '''
        
        if axis == 0:
            gridsize_adj = np.float32(stepsize) * self.dx 
            intaxis = self.xaxis
            self.projection_x_size = self.yaxis.size
            self.projection_y_size = self.zaxis.size
        elif axis == 1:
            gridsize_adj = np.float32(stepsize) * self.dy
            intaxis = self.yaxis
            self.projection_x_size = self.xaxis.size
            self.projection_y_size = self.zaxis.size
        else:
            gridsize_adj = np.float32(stepsize) 
            intaxis = self.zaxis 
            self.projection_x_size = self.xaxis.size
            self.projection_y_size = self.yaxis.size

        input_size = self.xaxis.size * self.yaxis.size * self.zaxis.size

        # This must be a multiple of xsize and ysize
        self.maxgridsize = gridsplit * self.projection_x_size * self.projection_y_size
        # gridsize_adj takes into account the grid spacing normalized with the number of 
        # CUDA steps and number of grid points along the LOS axis. 
        # splitsize is the number of grid points and stepsize is the substep
        # (from 0 to 1) in the cuda code, so, it needs to be * by splitsize
        # and stepsize. Note, splitsize is done in renderer/__init__.py

        dsc = np.ones_like(self.r) # dsc constains the grid spacing of the selected axis (in 3D array) 
        if axis == 0:
            dsc *= gridsize_adj
        elif axis == 1:
            dsc *= gridsize_adj
        else:
            for iix in range(self.projection_x_size):
                for iiy in range(self.projection_y_size):
                    dsc[iix,iiy,:] *=  (np.gradient(intaxis))[:] * gridsize_adj                  
                                          
        tables = [('atex', self.acont_tables[channel])]
   
        split_tables = [('dtex', self.r),
            ('entex', self.ne), 
            ('tgtex', self.tg), 
            ('dstex', dsc)]

        if do_mit is not None: 
            self.load_m2ratio(filename=do_mit)
            do_mit_tf = True
        else: 
            do_mit_tf = False
            
        consts = [('enmin', np.float32(self.enmin)),
                  ('enrange', np.float32(self.enrange)),
                  ('tgmin', np.float32(self.tgmin)),
                  ('tgrange', np.float32(self.tgrange))]

        if opacity:
            self.set_lambd(fw,channel)
            tables.append(('katex', self.ka_table)) #TABLE
            consts.append(('opatgmin',np.float32(self.opatab.teinit)))
            consts.append(('opatgrange',np.float32(self.opatab.dte*self.opatab.nte)))
        if do_mit is not None:
            split_tables.append(('modbtex',self.modb))
            tables.append(('m2tex',self.m2tex)) #TABLE
            consts.append(('modbrange', np.float32(self.modbrange)))
            consts.append(('modbmin', np.float32(self.modbmin))) 
        
        if not opacity or tau is None:
            tau = np.zeros((self.projection_x_size, self.projection_y_size),
                            dtype='float32')
        else:
            if tau.shape != (self.projection_x_size, self.projection_y_size):
                raise Exception('Tau must have shape ' + str(
                                self.projection_x_size) +
                                ' by ' + str(self.projection_y_size))
            tau = tau.astype('float32')

        tempout = np.empty_like(tau)
        
        def ispec_render(self, blocksize, gridsize):
            frender = self.mod.get_function('iRender')
            # integrates to find the emission
            tau = ispec_render.tauout
            frender(cuda.Out(tempout), cuda.InOut(tau), np.int8(opacity), np.int8(do_mit_tf),
                    block=(int(blocksize), 1, 1), grid=(int(gridsize), 1, 1))

            ispec_render.datout += tempout
            ispec_render.tauout = tau
        
        ispec_render.datout = np.zeros_like(tempout)
        ispec_render.tauout = np.zeros_like(tempout)
        
        self.render(90.0, 0.0 ,axis, reverse, gridsplit, consts, tables,
                    split_tables, ispec_render, stepsize, verbose, do_mit=do_mit_tf)

        return (ispec_render.datout, ispec_render.tauout)
    

    def il_render(self,channel, azimuth, altitude, axis, reverse=False,
                  nlamb=121, dopp_width_range=1e1,
                  tau=None, opacity=False, dnus=None, verbose=True,
                  gridsplit=20, fw=None, stepsize=0.001, do_mit=None):
        '''
        Computes the total intensity a long one of the selected axis in SE, 
        
         Parameters
        ----------
        channel - string
            Name of the spectral line to calculate. In order to know the f
            ormat, $BIFROST/PYTHON/br_int/br_ioni/data
            contains files with the G(T,ne), usually name.opy. spline must
            be name, e.g., 'fe_8_108.073'.
        azimuth - real
            Not used. This allows to trace rays for any angle. In this cases
            uses none sa modules. In this case, axis parameter is not need to be used.
        altitude - real
            Not used. This allows to trace rays for any angle. In this cases
            uses none sa modules. In this case, axis parameter is not need to be used.
        axis - integer number: 0 = x, 1 = y, 2 = z
            allows to chose the LOS integration axis (see azimuth and altitude)
        reverse - logical (False)
            Will integrate forward or backwards along the LOS. 
        nlamb - integer (121)
            number of spectral pixels. 
        dopp_width_range - real (10.)
            grid spacing in km/s in spectra.
        tau - 2D array (None)
            Initial tau. Default (None), means it is set to zero. 
        opacity - logical (False)
            Calcuate opacities.
        dnus - array (None)
            input spectra array in km/s.
        verbose - logical (False)
            provides some print outs. 
        fw -  real (None)
            allows setting a different wavelength for opacity calculations used for set_lambd function 
        gridsplit - integer (20)
            number of grid points along the LOS that will run the CUDA code at once. 
        stepsize - real (0.001)
            grid spacing in the CUDA code. 
        do_mit - string (Default None)
            MIT effect (work in progress)
            
        Returns
        -------
        array - ndarray
            Intensity array with the dimensions of the 3D spatial from the simulation in SI as a
            function of spectrum.
        array - ndarray
            spectrum in km/s
        real 
            wavelength center possition AA
        array - ndarray
            Intensity array with the dimensions of the 2D spatial from the simulation in cgs.
        '''

        if axis == 0:
            gridsize_adj = np.float32(stepsize) * self.dx 
            intaxis = self.xaxis
            self.projection_x_size = self.yaxis.size
            self.projection_y_size = self.zaxis.size
        elif axis == 1:
            gridsize_adj = np.float32(stepsize) * self.dy 
            intaxis = self.yaxis
            self.projection_x_size = self.xaxis.size
            self.projection_y_size = self.zaxis.size
        else:
            gridsize_adj = np.float32(stepsize) 
            intaxis = self.zaxis 
            self.projection_x_size = self.xaxis.size
            self.projection_y_size = self.yaxis.size

        input_size = self.xaxis.size * self.yaxis.size * self.zaxis.size

        # This must be a multiple of xsize and ysize
        self.maxgridsize = gridsplit * self.projection_x_size * self.projection_y_size

        # gridsize_adj takes into account the grid spacing normalized with the number of 
        # CUDA steps and number of grid points along the LOS axis. 
        # splitsize is the number of grid points and stepsize is the substep
        # (from 0 to 1) in the cuda code, so, it needs to be * by splitsize
        # and stepsize. Note, splitsize is done in renderer/__init__.py

        dopp_width0 = self.ny0[channel] / CC * m.sqrt(
                                            2 * KB / self.awgt[channel] / MP)

        dny = dopp_width_range * dopp_width0 * m.sqrt(TEMAX) / nlamb
        if dnus is None:
            dnus = np.empty(nlamb, dtype='float32')
            for i in range(0,nlamb):
                dnus[i] = (i - (nlamb - 1) / 2) * dny
        else:
            dnus = np.array(dnus, dtype='float32')

        dsc = np.ones_like(self.r) # dsc constains the grid spacing of the selected axis (in 3D array) 
        if axis == 0:
            dsc *= gridsize_adj
        elif axis == 1:
            dsc *= gridsize_adj
        else:
            for iix in range(self.projection_x_size):
                for iiy in range(self.projection_y_size):
                    dsc[iix,iiy,:] *=  (np.gradient(intaxis))[:] * gridsize_adj                  
                        
        tables = [('atex', self.acont_tables[channel])]
  
        split_tables = [('dtex', self.r),
            ('entex', self.ne), 
            ('uatex', self.uu),
            ('tgtex', self.tg), 
            ('dstex', dsc)]

        if do_mit is not None: 
            self.load_m2ratio(filename=do_mit)
            do_mit_tf = True
        else: 
            do_mit_tf = False
            
        consts = [('enmin', np.float32(self.enmin)),
                  ('enrange', np.float32(self.enrange)),
                  ('tgmin', np.float32(self.tgmin)),
                  ('tgrange', np.float32(self.tgrange))]
            
        if opacity:
            self.set_lambd(fw,channel)
            tables.append(('katex', self.ka_table)) #TABLE
            consts.append(('opatgmin',np.float32(self.opatab.teinit)))
            consts.append(('opatgrange',np.float32(self.opatab.dte*self.opatab.nte)))
            
        if do_mit is not None: 
            split_tables.append(('modbtex',self.modb))
            tables.append(('m2tex',self.m2tex))
            consts.append(('modbrange', np.float32(self.modbrange)))
            consts.append(('modbmin', np.float32(self.modbmin))) 

        reset_tau = False
        if not opacity or tau is None:
            tau_wvl = np.zeros((self.projection_x_size, self.projection_y_size, dnus.size), 
                            dtype='float32')
            tau = np.zeros((self.projection_x_size, self.projection_y_size), 
                            dtype='float32')
            reset_tau = True
                    
        else:
            if tau.shape != (self.projection_x_size, self.projection_y_size):
                raise Exception('Tau must have shape ' + str(
                                self.projection_x_size) +
                                ' by ' + str(self.projection_y_size))
            tau = tau.astype('float32')
            
        tempout = np.empty_like(tau)
        
        def ilspec_render(self, blocksize, gridsize):
            frender = self.mod.get_function('ilRender')
            for nfreq in range(0,dnus.size):
                tau = np.array(np.squeeze(ilspec_render.tauout[:,:,nfreq]),dtype='float32')
                
                frender(cuda.Out(tempout), cuda.In(dnus), cuda.InOut(tau),
                        np.float32(self.ny0[channel]), np.float32(dopp_width0),
                        np.int32(dnus.size), np.int8(opacity), np.int32(nfreq), np.int8(do_mit_tf),
                        block=(int(blocksize), 1, 1), grid=(int(gridsize), 1, 1))

                ilspec_render.tauout[:,:,nfreq] = tau
                ilspec_render.datout[:,:,nfreq] += tempout
                
        ilspec_render.datout = np.zeros((self.projection_x_size, self.projection_y_size, dnus.size), 
                            dtype='float32')
        ilspec_render.tauout = np.zeros((self.projection_x_size, self.projection_y_size, dnus.size), 
                            dtype='float32')

        self.render(90.0, 0.0 ,axis, reverse, gridsplit, consts, tables,
                    split_tables, ilspec_render, stepsize, verbose, do_mit=do_mit_tf)


        return (ilspec_render.datout, dnus, self.ny0[channel], ilspec_render.tauout)
    

    def channellist(self):
        return self.acont_filenames
    

    def set_lambd(self, lambd, channel):
        '''
        Set wavelength for calculating opacity table as a function of temperature.
        from anzer & heinzel apj 622: 714-721, 2005, march 20
        these clowns have a couple of great big typos in their reported c's.... correct values to 
        be found in rumph et al 1994 aj, 107: 2108, june 1994

        gaunt factors are set to 0.99 for h and 0.85 for heii, which should be good enough
        for the purposes of this code
        
        Parameters
        ----------
        lambda - real 
            wavelenght in AA 
        channel - integer 
            select the channel if a list is provided. 
        '''
        if self.opatab is None:
            self.opatab = Opatab(fdir=datapath)
        if lambd is None:
            lambd = CCA / self.ny0[channel]
        if lambd != self.prev_lambd:
            self.prev_lambd = lambd
        self.ka_table = self.opatab.h_he_absorb(lambd)
        self.ka_table = np.vstack([self.ka_table, self.ka_table])

    def load_m2ratio(self, filename='fe_10_257.259_mit.opy'):
        '''
        Reads save file from Enrico Landi for MIT effect.

        
        Parameters
        ----------
        filename - integer 
        '''
        
        filehandler = open(filename, 'rb')
        ion=pickle.load(filehandler)

        self.tgmin = np.min(np.log10(ion.Gofnt['temperature']))
        self.tgrange = np.max(np.log10(ion.Gofnt['temperature'])) - self.tgmin
        self.enmin = np.log10(np.min(ion.Gofnt['eDensity']))
        self.enrange = np.log10(np.max(ion.Gofnt['eDensity'])) - self.enmin
        
        self.m2tex = ion.Gofnt['gofnt']
        self.m2tex[np.where(ion.Gofnt['gofnt'] < 0)] = 0
        self.modbmin = np.min(ion.Gofnt['b_gauss'])
        self.modbrange = np.max(ion.Gofnt['b_gauss']) - self.modbmin
        

class SATDIEmRenderer(SAEmissivityRenderer):
    '''
    Class for rendering emissions of Bifrost numerical model
    using calculated NEQ ion densities, i.e. OOE module for a single
    axis (x, y or z).
    '''
    #cache emissivity for speed when rendering
    level = -1
    em = None

    def __init__(self, data_dir='./', paramfile='oxygen-II-VII-iris',
                 snap=None):
        '''
        Initializes renderer, and loads data from a directory and paramfile.
        If snap is none, picks the earliest snap specified by gpuparam.txt.
        '''
        super(SATDIEmRenderer, self).__init__(SATDICUDACODE, data_dir=data_dir)

        irisfile = open(data_dir + '/' + paramfile)
        data = [line for line in irisfile.readlines() if line[0] != '*']

        self.element_name = data.pop(0).strip()
        self.ab, self.awgt = (float(i) for i in data.pop(0).split())
        nk, nlines, _, _ = (int(i) for i in data.pop(0).split())
        self.egis = []  # array of (ev, g, ion)
        self.trns = []

        for _ in range(0,nk):
            datstring = shlex.split(data.pop(0))
            ev = float(datstring[0]) * CC * HH / EE
            g = float(datstring[1])
            label = datstring[2]
            ion = int(datstring[3])
            self.egis.append(Egi(ev, g, label, ion))

        for _ in range(0,nlines):
            j, i, f, _, _, _, _, _, _, _ = (
                                        float(i) for i in data.pop(0).split())
            j = int(j)
            i = int(i)

            dn, up = i, j
            if self.egis[j - 1].ev < self.egis[i - 1].ev:
                dn, up = j, i
            irad = dn - 1
            jrad = up - 1

            alamb = HCE / (self.egis[jrad].ev - self.egis[irad].ev)
            a_ul = f * 6.6702e15 * self.egis[irad].g / (
                                                self.egis[jrad].g * alamb ** 2)

            self.trns.append(Trn(irad, jrad, alamb, a_ul, f))

    def i_render(self, level, azimuth, altitude, axis, reverse,
                 gridsplit=20, tau=None, opacity=False, verbose=True,
                 fw=None, stepsize=0.001):

        if level != self.level:
            self.em = self.get_emissivities(level)
            self.level = level
            
        if axis == 0:
            gridsize_adj = np.float32(stepsize) * self.dx
            intaxis = self.xaxis
            self.projection_x_size = self.yaxis.size
            self.projection_y_size = self.zaxis.size
        elif axis == 1:
            gridsize_adj = np.float32(stepsize) * self.dy 
            intaxis = self.yaxis
            self.projection_x_size = self.xaxis.size
            self.projection_y_size = self.zaxis.size
        else:
            gridsize_adj = np.float32(stepsize)
            intaxis = self.zaxis 
            self.projection_x_size = self.xaxis.size
            self.projection_y_size = self.yaxis.size

        input_size = self.xaxis.size * self.yaxis.size * self.zaxis.size

        # This must be a multiple of xsize and ysize
        self.maxgridsize = gridsplit * self.projection_x_size * self.projection_y_size
        # gridsize_adj takes into account the grid spacing normalized with the number of 
        # CUDA steps and number of grid points along the LOS axis. 
        # splitsize is the number of grid points and stepsize is the substep
        # (from 0 to 1) in the cuda code, so, it needs to be * by splitsize
        # and stepsize. Note, splitsize is done in renderer/__init__.py

        consts = []
        tables = []
      
        dsc = np.ones_like(self.r) # dsc constains the grid spacing of the selected axis (in 3D array) 
        if axis == 0:
            dsc *= gridsize_adj
        elif axis == 1:
            dsc *= gridsize_adj
        else:
            for iix in range(self.projection_x_size):
                for iiy in range(self.projection_y_size):
                    dsc[iix,iiy,:] *=  (np.gradient(intaxis))[:] * gridsize_adj
        
        split_tables = [('emtex', self.em), 
                       ('dstex', dsc)]

        count=0
        for name, table in split_tables:
            if name == 'emtex':
                ipos = count
            count+=1

        if opacity:
            self.set_lambd(fw,level)
            tables.append(('katex', self.ka_table))
            consts.append(('opatgmin',np.float32(self.opatab.teinit)))
            consts.append(('opatgrange',np.float32(self.opatab.dte*self.opatab.nte)))
            split_tables.extend([('dtex', self.r),
                                ('eetex', self.e)])
            consts.extend([('dmin', np.float32(self.dmin)),
                           ('drange', np.float32(self.drange)),
                           ('emin', np.float32(self.emin)),
                           ('erange', np.float32(self.erange))])

        if not opacity or tau is None:
            tau = np.zeros((self.projection_x_size, self.projection_y_size),
                            dtype='float32')
        else:
            if tau.shape != (self.projection_x_size, self.projection_y_size):
                raise Exception('Tau must have shape ' + str(
                                self.projection_x_size) +
                                ' by ' + str(self.projection_y_size))
            tau = tau.astype('float32')

        tempout = np.empty_like(tau)
        
        opa_int = 0 
        if (opacity): 
            opa_int = 1
            
        def ispec_render(self, blocksize, gridsize):
            frender = self.mod.get_function('iRender')
            frender(cuda.Out(tempout), cuda.InOut(tau), np.int8(opa_int),
                    block=(int(blocksize), 1, 1), grid=(int(gridsize), 1, 1))
            ispec_render.datout += tempout

        ispec_render.datout = np.zeros_like(tempout)

        self.render(0, 0, axis, reverse, gridsplit, consts, tables,
                    split_tables, ispec_render, stepsize, verbose)

        return (ispec_render.datout, tau)

    def il_render(self, level, azimuth, altitude, axis, reverse,
                  nlamb=121, dopp_width_range=1e1, tau=None, opacity=False,
                  dnus=None, verbose=True, gridsplit=20, fw=None,
                  stepsize=0.001):

        if level != self.level:
            self.em = self.get_emissivities(level)
            self.level = level

        if axis == 0:
            gridsize_adj = np.float32(stepsize) * self.dx
            intaxis = self.xaxis
            self.projection_x_size = self.yaxis.size
            self.projection_y_size = self.zaxis.size
        elif axis == 1:
            gridsize_adj = np.float32(stepsize) * self.dy 
            intaxis = self.yaxis
            self.projection_x_size = self.xaxis.size
            self.projection_y_size = self.zaxis.size
        else:
            gridsize_adj = np.float32(stepsize) 
            intaxis = self.zaxis 
            self.projection_x_size = self.xaxis.size
            self.projection_y_size = self.yaxis.size

        input_size = self.xaxis.size * self.yaxis.size * self.zaxis.size

        # This must be a multiple of xsize and ysize
        self.maxgridsize = gridsplit * self.projection_x_size * self.projection_y_size
        # gridsize_adj takes into account the grid spacing normalized with the number of 
        # CUDA steps and number of grid points along the LOS axis. 
        # splitsize is the number of grid points and stepsize is the substep
        # (from 0 to 1) in the cuda code, so, it needs to be * by splitsize
        # and stepsize. Note, splitsize is done in renderer/__init__.py

        if opacity:
            self.set_lambd(fw,channel)
            tables.append(('katex', self.ka_table)) #TABLE
            consts.append(('opatgmin',np.float32(self.opatab.teinit)))
            consts.append(('opatgrange',np.float32(self.opatab.dte*self.opatab.nte)))
            
        dsc = np.ones_like(self.r) # dsc constains the grid spacing of the selected axis (in 3D array) 
        if axis == 0:
            dsc *= gridsize_adj
        elif axis == 1:
            dsc *= gridsize_adj
        else:
            for iix in range(self.projection_x_size):
                for iiy in range(self.projection_y_size):
                    dsc[iix,iiy,:] *=  (np.gradient(intaxis))[:] * gridsize_adj      
        
        ny0 = CCA / self.trns[level].alamb
        dopp_width0 = ny0 / (CC / 1e2) * m.sqrt(2 * KB / self.awgt[level] / MP)

        dny = dopp_width_range * 1.0 / nlamb * dopp_width0 * m.sqrt(TEMAX)
        if dnus is None:
            dnus = np.empty(nlamb, dtype='float32')
            for i in range(0,nlamb):
                dnus[i] = (i - (nlamb - 1) / 2) * dny
        else:
            dnus = np.array(dnus, dtype='float32')

        consts = [('dmin', np.float32(self.dmin)),
                  ('drange', np.float32(self.drange)),
                  ('emin', np.float32(self.emin)),
                  ('erange', np.float32(self.erange))]
        #tables = [('tgtex', self.tg_table)]
        split_tables = [('emtex', self.em),
                        ('uatex', self.uu),
                        ('tgtex', self.tg),
                        ('dtex', self.r),
                        ('dstex', dsc)]
        
        if not opacity or tau is None:
            tau = np.zeros((self.projection_x_size, self.projection_y_size),
                            dtype='float32')
        else:
            if tau.shape != (self.projection_x_size, self.projection_y_size):
                raise Exception('Tau must have shape ' + str(
                                self.projection_x_size) +
                                ' by ' + str(self.projection_y_size))
            tau = tau.astype('float32')

        tempout2 = np.empty_like(np.empty(tau.shape + (dnus.size,),
                                 dtype='float32'))
        tempout = np.empty_like(tau)
        
        opa_int = 0 
        if (opacity): 
            opa_int = 1
            
        # scope hackery, we're not in python 3
        def ilspec_render(self, blocksize, gridsize):
            frender = self.mod.get_function('ilRender')
            for nfreq in range(0,dnus.size):
                frender(cuda.Out(tempout), cuda.In(dnus), cuda.InOut(tau),
                        np.float32(self.ny0[channel]), np.float32(dopp_width0),
                        np.int32(dnus.size), np.int8(opa_int),np.int32(nfreq),
                        block=(int(blocksize), 1, 1),
                        grid=(int(gridsize), 1, 1))

                tempout2[:,:,nfreq]+= tempout
            ilspec_render.datout= tempout2

        self.render(0, 0, axis, reverse, gridsplit, consts, tables,
                    split_tables, ilspec_render, stepsize, verbose)
 
        return (ilspec_render.datout, dnus, tau)

    def get_emissivities(self, level=None):
        '''
        Returns a 3d table giving the emissivity at each point.
        If level is None, prompts will guide you through selecting
        a level.
        '''
        if (level is None):
            tion = int(input('Input the oxygen ionization level to check: '))
            ionln = [i for i, v in enumerate(self.egis) if v.ion == tion]

            print('These are the ' + self.element_name + str(tion) + ' lines:')
            print('nr\tlvl l\tlvl u\tname lvl l\tname lvl u\twavelength\tEinstein Aij\tion')
            print('--------------------------------------------------------------------------------')

            for n, t in enumerate(self.trns):
                for ind in ionln:
                    if t.jrad == ind or t.irad == ind:
                        print(str(n) + ')\t' + str(t.irad + 1) + '\t' + str(t.jrad + 1) + '\t' +
                              self.egis[t.irad].label + '\t' + self.egis[t.jrad].label + '\t' +
                              '%e' % t.alamb + '\t' + '%e' % t.a_ul + '\t' + str(self.egis[t.irad].ion))
                        break

            level = int(input('Choose a line: '))

        tline = self.trns[level]
        ion_densities = self.BifrostData.getooevar(tline.jrad)
        return ((HH * CC * 1e-2 / (tline.alamb * 1e-10) * tline.a_ul / (
                                4 * m.pi)) * ion_densities)[..., :self.locph]

    def set_lambd(self, lambd, level):
        '''
        Set wavelength for calculating opacity table as a function of temperature.
        from anzer & heinzel apj 622: 714-721, 2005, march 20
        these clowns have a couple of great big typos in their reported c's.... correct values to 
        be found in rumph et al 1994 aj, 107: 2108, june 1994

        gaunt factors are set to 0.99 for h and 0.85 for heii, which should be good enough
        for the purposes of this code
        
        Parameters
        ----------
        lambda - real 
            wavelenght in AA 
        channel - integer 
            select the channel if a list is provided. 
        '''
        if self.opatab is None:
            self.opatab = Opatab(fdir=datapath)
        if lambd is None:
            lambd = self.trns[level].alamb
        if lambd != self.prev_lambd:
            self.prev_lambd = lambd
        self.ka_table = self.opatab.h_he_absorb(lambd)


    def load_m2ratio(self, filename='fe_10_257.259_mit.opy', iwvl=2830):
        '''
        Reads save file from Enrico Landi for MIT effect.

        
        Parameters
        ----------
        filenme - integer 
        '''
        
        filehandler = open(filename, 'rb')
        ion=pickle.load(filehandler)

        self.tgmin = np.min(np.log10(ion.Gofnt['temperature']))
        self.tgrange = np.max(np.log10(ion.Gofnt['temperature'])) - self.tgmin
        self.enmin = np.log10(np.min(ion.Gofnt['eDensity']))
        self.enrange = np.log10(np.max(ion.Gofnt['eDensity'])) - self.enmin
        
        self.m2tex = ion.Gofnt['gofnt']
        self.m2tex[np.where(ion.Gofnt['gofnt'] < 0)] = 0
        self.modbmin = np.min(ion.Gofnt['b_gauss'])
        self.modbrange = np.max(ion.Gofnt['b_gauss']) - self.modbmin
        
        
def mult_render(self, renderer, sdomain, edomain, step=1, lambd=None,
                channel=0, il_render=False, opacity=True, curvature=False,
                fw=None, along_x=True, gridsplit=20, stepsize=0.001):
    '''
    Uses a renderer to render a domain multiple times, and stack it on top of
    itself repeatedly.
    [sdomain, edomain] is the range of timestamps to use
    '''
    tau = None
    emiss = 0
    altitude = 0

    renderer.x_pixel_offset = renderer.y_pixel_offset = 0
    renderer.update_axes(0.0)

    mdomain = (sdomain + edomain) / 2
    zrange = np.ptp(renderer.zaxis)
    dtheta = m.atan(np.ptp(renderer.yaxis) / RSUN)

    renderer.set_lambd(lambd,channel)

    for cdomain in range(sdomain, edomain + 1, step):
        renderer.set_snap(cdomain)

        offset = 0
        if curvature:  # tilt/shift
            altitude = dtheta * (mdomain - cdomain) / step
            offset = (zrange / 2 + RSUN) * (m.cos(altitude) - 1)
            renderer.y_pixel_offset = offset / renderer.distance_per_pixel
        print("Rendering time " + str(cdomain) + " with altitude " +
              str(m.degrees(altitude)) + " offset " + str(offset),end="\r",flush=True)

        if il_render:
            output = renderer.il_render(channel, 0 if along_x else 90,
                                        altitude,0,0, gridsplit=gridsplit,
                                        tau=tau,
                                        opacity=opacity, fw=fw,
                                        stepsize=stepsize)
            if opacity:
                demiss, _, tau = output
            else:
                demiss, _ = output
        else:
            output = renderer.i_render(channel, 0 if along_x else 90,
                                       altitude, axis, 0, gridsplit=gridsplit,
                                       tau=tau, opacity=opacity, fw=fw,
                                       stepsize=stepsize)
            if opacity:
                demiss, tau = output
            else:
                demiss = output

        emiss += demiss

    return emiss

def noisify_spectra(spectra, snr, gaussian=True):
    '''
    Adds Poisson noise to spectra. SNR specified in decibels.

    gaussian specifies whether or not noise is generated according to a
    gaussian distribution--
    if false, noise is uniformly distributed.
    '''
    if snr <= 0.0:
        return spectra

    nsr = 10 ** -(snr / 10.0)
    if gaussian:
        noise = np.sqrt(spectra) * (NOISEGEN.gen_normal(
                                    spectra.shape, 'float32') * nsr).get()
    else:
        noise = np.sqrt(spectra) * ((NOISEGEN.gen_uniform(
                                spectra.shape, 'float32') * 2 - 1) * nsr).get()

    return spectra + noise


def savearray(name, array):
    '''
    Saves an array in a binary format.
    File has format (int number of dimensions), (int xsize), (int ysize) ....
    (int lastdimensionsize),
    (lots of float32s that make up the remainder of the data,
    in C array format).
    '''

    with open(name, mode='wb') as newfile:
        newfile.write(bytes(np.array((len(array.shape), ) + array.shape,
                      dtype='int32').data))
        newfile.write(bytes(array.data))


def loadarray(name):
    '''
    Loads an array saved with savearray.
    Honestly, if you are just going to use python, please just use np.save()
    and np.load().
    '''
    header = np.memmap(name, dtype='int32', offset=0, shape=(1,), mode='r')
    ndims = header[0]
    header = np.memmap(name, dtype='int32', offset=0, shape=(1 + ndims,),
                       mode='r')
    restofdata = np.memmap(name, dtype='float32', offset=4 * (ndims + 1),
                           shape=tuple(header[1:]), mode='r')
    header.flush()
    return restofdata
